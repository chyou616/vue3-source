// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2
// }

// const r = reactive(obj);
// console.log(r.a);

// r.a = 10;


// 如果代理的是同一个对象的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2
// }

// const state1 = reactive(obj);
// const state2 = reactive(obj);

// console.log(state1 === state2);


// 如果对代理对象再进行代理的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2
// }

// const state1 = reactive(obj);
// const state2 = reactive(state1);

// console.log(state1 === state2);


import { reactive } from "./reactive";

const obj = {
  a: 1,
  b: 2,
  get c() { 
    console.log('get c', this);
    return this.a + this.b;
  }
}

const state1 = reactive(obj);

function fn() { 
  state1.c;
}

fn();