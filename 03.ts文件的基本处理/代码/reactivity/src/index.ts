import { reactive } from "./reactive";

const obj = {
  a: 1,
  b: 2
}

const r = reactive(obj);
console.log(r.a);

r.a = 10;