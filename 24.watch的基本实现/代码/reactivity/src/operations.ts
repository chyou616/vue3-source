export const enum TrackOpTypes{
  GET = 'GET',
  HAS = 'HAS',
  ITERATE = 'ITERATE'
}

export const enum TriggerOpTypes { 
  SET = 'SET',
  ADD = 'ADD',
  DELETE = 'DELETE'
}