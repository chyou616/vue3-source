## watch的基本实现

### 1、watch的简单封装

vue的响应式其实还有一个经常被提起的API，那就是`watch`。不过在vue3源码中，watch相关的代码是放到了`runtime`模块中进行封装的，watch实际上就是对effect函数的二次封装。

我们先根据之前的代码，来进行简单的js封装。看一下实际的效果。

首先考虑的当然就是说Object对象类型的处理。其实我们只需要监听的是一个对象，当这个对象的某个属性被捕获，那么我们就执行一个回调函数。而且我们之前也说了，watch实际上是对effect的二次封装。那最简单的处理，我们只需要在watch中调用effect函数，直接让effect函数帮我们进行处理

```typescript
const obj = {
  name: "张三",
  age: 18
};
function watch(source, cb) {
  effect(() => source.name, {
    scheduler() {
      cb();
    },
  });
}

const proxy = reactive(obj);

watch(proxy, () => {
  console.log("---修改了obj的name属性---");
});

effect(() => {
  layer1.innerHTML = proxy.name;
});

btn1.addEventListener("click", () => {
  proxy.name = "王五";
});
```

当然这个最简单的watch，直接固定了监听的只是某个对象的name属性，这肯定是不对的。那为了让 watch 函数具有通用性，我们需要一个封装一个通用的读取操作。

### 2、属性的通用读取操作

```typescript
function traverse(value, seen = new Set()) {
  // 如果要读取的数据是原始值，或者已经被读取过了，那么什么都不做
  if (!isObject(value) || seen.has(value)) {
    return;
  }
  // 将数据添加到 seen 中，代表遍历地读取过了，避免循环引用引起的死循环
  seen.add(value);

  if (isRef(value)) {
    traverse(value.value, seen)
  } else if (isArray(value)) {
    for (let i = 0; i < value.length; i++) {
      traverse(value[i], seen)
    }
  }
  // 还需判断是否是Map和Set...这里省略
  else { 
    // 假设 value 就是一个对象，使用 for...in 读取对象的每一个值，
    // 并递归地调用 traverse 进行处理
    for (const k in value) {
      traverse(value[k], seen);
    }
  }
  return value;
}

function watch(source, cb) {
  effect(() => traverse(source), {
    scheduler() {
      cb();
    },
  });
}
```

当然，现在这个简单的watch，我们还有很多边界要判断，比如：**watch的第一个参数可以是getter，也可以是一个对象，也有可能传递的就是一个原始值，对象可能是ref，也有可能是reactive，也有可能是一个数组**

```typescript
function watch(source, cb) {
  let getter;
  
  if (isFunction(source)) { 
    getter = source
  }
  else {
    getter = () => traverse(source)
  }

  
  effect(() => getter(), {
    scheduler() {
      cb();
    },
  });
}
```

### 3、新值与旧值

通常我们在使用 `Vue.js` 中 的 `watch` 函数时，能够在**回调函数中得到变化前后的值**

那么如何获得新值与旧值呢?这需要充分利用 `effect` 函数的 `lazy` 选项

```typescript
function watch(source, cb) {
  // ...上面的代码省略...

  // 定义旧值与新值
  let oldValue, newValue;

  // 使用 effect 注册副作用函数时，开启 lazy 选项，并把返回值存储到
  // effectFn 中以便后续手动调用
  const effectFn = effect(() => getter(), {
    lazy: true,
    scheduler() {
      // 得到新值
      newValue = effectFn();
      cb(newValue, oldValue);
      // 更新旧值，下一次获取就可以得到旧值
      oldValue = newValue;
    },
  });
  oldValue = effectFn();
}
```

**测试代码：**

```typescript
const proxy = reactive(obj);

watch(()=>proxy.name, (newValue, oldValue) => {
  console.log("---修改了obj的name属性---");
  console.log("---oldValue---", oldValue);
  console.log("---newValue---", newValue);
});

effect(() => {
  layer1.innerHTML = proxy.name;
});

btn1.addEventListener("click", () => {
  proxy.name = "王五";
});

btn2.addEventListener("click", () => {
  proxy.name = "赵六";
});

```

### 4、立即执行的watch

默认情况下，一个 `watch` 的回调只会在响应式数据发生变化时才执行

在 Vue.js 中可以通过选项参数 `immediate` 来指定回调是否需要立即执行。

```typescript
<script setup lang="ts">
  import { ref, watch } from "vue";
type Person = {
  name: string;
  age: number;
};

const person = ref<Person>({ name: "jack", age: 18 });

const changePerson = () => {
  person.value.name = "lucy";
};
watch(()=>person.value.name, () => { 
  console.log(person.value.name);
}, {
  immediate: true
})
</script>

<template>
  <div>
    <h2>{{ person.name }}---{{ person.age }}</h2>
    <button @click="changePerson">修改</button>
  </div>
</template>
```

当 `immediate` 选项存在并且为 `true` 时，回调函数会在该 `watch` 创建时立刻执行一次。仔细思考就会发现，回调函数的立即执 行与后续执行本质上没有任何差别，所以我们可以把 `scheduler` 调 度函数封装为一个通用函数，分别在初始化和变更时执行它。

```typescript
function watch(source, cb, options = {}) {
  let getter;

  if (isFunction(source)) {
    getter = source;
  } else {
    getter = () => traverse(source);
  }

  // 定义旧值与新值
  let oldValue, newValue;

  // 将调度函数scheduler内执行的代码单独封装为job函数
  const job = () => { 
    newValue = effectFn();
    cb(newValue, oldValue);
    oldValue = newValue;
  }

  // 使用 effect 注册副作用函数时，开启 lazy 选项，并把返回值存储到
  // effectFn 中以便后续手动调用
  const effectFn = effect(() => getter(), {
    lazy: true,
    scheduler:job,
  });

  if (options.immediate) { 
    job();
  }
  else {
    oldValue = effectFn();
  }
}

```
