export {
  createRenderer,
  options,
  shouldSetAsProps,
  normalizeClass,
  lis,
  render,
} from "./renderer";
export { watch } from "./apiWatch";
export { h } from "./h";
