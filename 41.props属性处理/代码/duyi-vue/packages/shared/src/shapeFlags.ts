export const enum ShapeFlags {
  ELEMENT = 1,
  FUNCTIONAL_COMPONENT = 1 << 1, // 2
  STATEFUL_COMPONENT = 1 << 2, // 4
  TEXT_CHILDREN = 1 << 3, // 8
  ARRAY_CHILDREN = 1 << 4, // 16
  SLOTS_CHILDREN = 1 << 5, // 32
  TELEPORT = 1 << 6, // 64
  SUSPENSE = 1 << 7, // 128
  COMPONENT_SHOULD_KEEP_ALIVE = 1 << 8, // 256
  COMPONENT_KEPT_ALIVE = 1 << 9, // 512
  COMPONENT = ShapeFlags.STATEFUL_COMPONENT | ShapeFlags.FUNCTIONAL_COMPONENT // 6
}

/*
按位或 | 运算符
   0001    -------1
|  1000    -------8
----------
   1001    -------9

   00001   -------1
|  10000   -------16
----------
   10001   -------17



// 如果要做判断的时候，可以和 按位与 & 运算符结合使用

   1001    -------9
&  0001    -------1
----------
   0001    -------1   // 只要有值，可以表明组成关系

   1001    -------9
&  0010    -------2
----------
   0000    -------0
*/
