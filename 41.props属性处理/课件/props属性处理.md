## props处理

这里只是简单处理一下props，实际props在vue3源码中还有很多边界判断，包括这里也涉及到了attrs属性，就不再一一去介绍了。

```typescript
function resolveProps(options:any, propsData:any) { 
    const props:any = {};
    const attrs:any = {};

    for (const key in propsData) {
      if (key in options) {
        props[key] = propsData[key];
      }
      else { 
        attrs[key] = propsData[key];
      }
    }

    return [props, attrs];
  }

  function mountComponent(vnode:any, container:any, anchor:any) {
    const componentOptions = vnode.type;
    const { render, data,props:propsOption, beforeCreate, created, beforeMount, mounted, beforeUpdate, updated  } = componentOptions;
    
    // 调用beforeCreate生命周期函数
    beforeCreate && beforeCreate();

    const state = reactive(data());

    // 调用resolveProps函数，将propsData分解为props和attrs
    const [props, attrs] = resolveProps(propsOption, vnode.props);

    // 定义组件实例，本质就是一个对象，用来存储与组件相关的状态
    const instance = {
      state,
      props:shallowReactive(props),
      isMounted: false,
      subTree: null,
    }

    // 将instance挂载到vnode上
    vnode.component = instance;

    const renderContext = new Proxy(instance, {
      get(target, key, receiver) {
        const { state, props } = target;
        if(state && key in state) {
          return state[key];
        }
        else if(key in props) {
          return props[key];
        }
        else {
          console.error("不存在")
        }
      },
      set(target, key, value, receiver) {
        const { state, props } = target;
        if (state && key in state) {
          state[key] = value;
        }
        else if (key in props) { 
          console.warn("props是只读的")
        }
        else { 
          console.error("不存在")
        }
        return true;
      }
    })

    // 调用created生命周期函数
    created && created.call(renderContext);

    effect(() => {
      const subTree = render.call(renderContext, renderContext);
      // 检查组件是否已经被挂载了
      if (!instance.isMounted) {
        // 调用beforeMount生命周期函数
        beforeMount && beforeMount.call(renderContext);

        // 初次挂载，patch函数第一个参数当然为null
        patch(null, subTree, container, anchor);
        // 挂载完成后，将isMounted设置为true
        instance.isMounted = true;

        // 调用mounted生命周期函数
        mounted && mounted.call(renderContext);
      }
      else {
        
        // 调用beforeUpdate生命周期函数
        beforeUpdate && beforeUpdate.call(renderContext);

        // 第一个参数为组件上一次渲染的subTree
        patch(instance.subTree, subTree, container, anchor);

        // 调用updated生命周期函数
        updated && updated.call(renderContext);
      }
      // 更新subTree
      instance.subTree = subTree;
      
    }, {
      scheduler: queueJob
    })
  }
```

测试：

```typescript
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h} = VueRuntimeCore
  

  const MyComponent = {
    name: 'MyComponent',
    data(){
      return {
        foo:'hello world'
      }
    },
    props:{
      title:String
    },
    render(){
      return h("div",{},[
        h("h2",{},'标题内容是--->' + this.title),
        h("span",{},'我是文本内容--->' + this.foo),
        h("button",{
          onClick:()=>{
            console.log('click')
            this.foo = 'hello vue'
          }
        },'按钮')
      ])
    }
  }

  render(h(MyComponent,{title:"我是title"}),document.getElementById('app'))
</script>
```



## props更新

父组件中，props的数据有可能会被修改，比如下面的情况：

```typescript
render(h(MyComponent,{title:"我是title"}),document.getElementById('app'))

setTimeout(()=>{
  render(h(MyComponent,{title:"我是title2"}),document.getElementById('app'))
},2000)
```

那么在更新的过程中，我们也需要对子组件进行更新，添加`patchComponent`函数进行处理

```typescript
// 其他省略
function patch(oldVNode: any, newVNode:any, container:any, anchor = null) {
  // 其他省略
  else if (typeof type === "object") {
    if (!oldVNode) {
      // 挂载组件
      mountComponent(newVNode, container, anchor);
    }
    else { 
      patchComponent(oldVNode, newVNode, anchor);
    }
  }
}
```

其实主要就是检测props是否需要被重新赋值，因为之前的`instance.props`对象我们本身就做的是**浅响应式对象**(shallowReactive)，所以，只需要设置`instance.props`对象下的属性值，就可以触发组件重新渲染

```typescript
function patchComponent(oldVNode:any, newVNode:any, anchor:any) { 
  // 获取组件实例，同时让新的组件虚拟节点newVNode.component也指向组件实例
  const instance = (newVNode.component = oldVNode.component);
  // 获取当前props 数据
  const { props } = instance;

  // 判断props是否发生变化，如果没有变化，不需要更新
  if (hasPropsChanged(oldVNode.props, newVNode.props)) { 
    // 调用resolveProps函数，重新获取props数据
    const [nextProps] = resolveProps(newVNode.type.props, newVNode.props);

    // 更新props
    for (const key in nextProps) { 
      props[key] = nextProps[key];
    }

    // 删除不存在的props
    for (const key in props) {
      if (!(key in nextProps)) {
        delete props[key];
      }
    }
  }
}

function hasPropsChanged(prevProps: any, nextProps: any) { 
  const prevKeys = Object.keys(prevProps);
  const nextKeys = Object.keys(nextProps);

  // 如果新旧props的key数量不一样，说明props发生了变化
  if (nextKeys.length !== prevKeys.length) { 
    return true;
  }

  // 遍历旧props，判断是否有key对应的value发生了变化
  for (let i = 0; i < nextKeys.length; i++) { 
    const key = nextKeys[i];
    if (nextProps[key] !== prevProps[key]) { 
      return true;
    }
  }

  return false;

}
```

