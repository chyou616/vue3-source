#### effect嵌套computed

如果出现下面的情况，也会出现问题：

```typescript
const res = computed(() => proxy.age + 10);

effect(() => {
  console.log(res.value);
});

// 修改代理对象属性的值
proxy.age++;
```

在副作用函数effect中，调用了计算属性的值，当代理对象属性改变的时候，按照我们想象的来说，应该要重新触发渲染，但是这里却没有任何反应。

其实这里的**本质就是一个effect嵌套**，我们现在的`computed`内部虽然有自己的`effect`，并且还是懒加载的，只有真正读取计算属性的值的时候才会执行。对于计算属性内部的getter函数来说，它里面访问的响应式数据只会把computed内部effect作为依赖收集的参考。

所以现在**问题的关键就指向了value在外部并不能触发依赖收集的问题**，其实就是下面的图：

![image-20240905102312529](./assets/image-20240905102312529.png)

那还不简单么，我们在计算属性内部手动触发一下就行了。

```diff
function computed(getterOrOptions) {
  let value;
  // dirty标志，用来标识是否需要重新计算，初始值为true，表示需要重新计算
  let dirty = true;

  const effectFn = effect(getterOrOptions, {
    lazy: true,
    // 添加调度器，在调度器中将dirty设置为true
    scheduler: () => {
      dirty = true;
+      trigger(obj, "value")
    },
  });

  const obj = {
    get value() {
      // 如果dirty为true,则重新计算value的值
      if (dirty) {
        console.log("---obj.value---");
        value = effectFn();
        // 计算完成后，将dirty设置为false,下一次直接使用缓存的value值
        dirty = false;
      }
+      track(obj, "value");
      return value;
    },
  };

  return obj;
}
```

#### 重新封装computed

现在我们的computed相当于接收了一个`getter`访问器属性，其实我们在使用computed的时候，可以人为的传递包含`getter`/`setter`的对象。但是现在的computed相当于只能接收一个回调函数，也就是一个`getter`，我们把computed函数再改造一下

```typescript
const isFunction = val => typeof val === 'function'
const NOOP = () => {}

function computed(getterOrOptions) { 
  let getter;
  let setter;

  if (isFunction(getterOrOptions)) {
    getter = getterOrOptions
    setter = NOOP
  } else {
    getter = getterOrOptions.get
    setter = getterOrOptions.set
  }

  return new ComputedRefImpl(getter, setter)
}

class ComputedRefImpl { 
  _value;
  _dirty = true;
  effect;
  _setter;

  constructor(getter, _setter) {
    this._setter = _setter
    this.effect = effect(getter, {
      lazy: true,
      scheduler: () => {
        if (!this._dirty) {
          this._dirty = true;
          trigger(this, "value")
        }
      },
    });
  }

  get value() {
    if (this._dirty) {
      this._value = this.effect()
      this._dirty = false
    }
    track(this, 'value')
    return this._value
  }

  set value(newValue) {
    this._setter(newValue)
  }
}
```

我们使用代码来测试一下：

```typescript
const obj = {
  firstName: "张",
  lastName: "三",
};
const proxy = new Proxy(obj, handler);
// ......其他代码省略

const res = computed({
  get: () => proxy.firstName + proxy.lastName,
  set: (val) => {
    const names = val.split('')
    proxy.firstName = names[0]
    proxy.lastName = names[1]
  }
});


effect(() => { 
  layer1.innerHTML = res.value
  layer2.innerHTML = proxy.firstName + "---" + proxy.lastName
})

btn1.addEventListener('click', () => {
  proxy.firstName = '李'
  proxy.lastName = '四'
})

btn2.addEventListener("click", () => { 
  res.value = "王五";
})
```

其实对于`computed`计算属性，**多数情况下，计算属性本身我们应该默认是只读的**，除非我们确实手动传入了`setter`。所以，我们最好也在没有传入setter的情况下，设置其为只读的。不过这个问题我们一起融入到TS代码中一并解决