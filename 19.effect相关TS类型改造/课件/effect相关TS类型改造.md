## 一、用到的数据变量

我们在之前的代码中，直接声明的变量有下面这几个：

```typescript
let activeEffect;
const effectStack = [];
const buckets = new WeakMap();
```

`activeEffect`非常特殊，首先他可以是一个**函数**，然后我们在它上面**挂载了另外一个数据结构deps**，其实也就是`Set`集合中所有的`activeEffect`，其次还挂载了一个`options`，而`options`里面还有`lazy`和`scheduler`，`scheduler`是一个函数，它本身是可以将一个副作用函数作为参数传递进去的。

而`effectStack`其实就是`activeEffect`类型的数组

因此，这一连串的内容，声明下面的**TS类型**

```typescript
export interface ReactiveEffect<T = any> {
  (): T
  deps: Array<Dep>
  options: ReactiveEffectOptions
}

type Dep = Set<ReactiveEffect>

export interface ReactiveEffectOptions {
  lazy?: boolean
  scheduler?: (job: ReactiveEffect) => void
}

const effectStack: ReactiveEffect[] = []
let activeEffect: ReactiveEffect | undefined
```

`const buckets = new WeakMap();`为了和源代码同步，我们将这个变量的名字改一下，就命名成`targetMap`，而且这个变量也是我们数据结构的起点，回忆之前的图：

![image-20240906092537051](./assets/image-20240906092537051.png)

`WeakMap`里面的`value`又是`Map`，`Map`里面的`value`又是装了副作用函数的`Set`，刚好和上面联系到了一起。

```typescript
type KeyToDepMap = Map<any, Dep>
const targetMap = new WeakMap<any, KeyToDepMap>()
```

## 二、track函数的处理

track函数我们几乎不需要做多余的改动，直接将之前写的代码，复制粘贴到ts代码中就行了。当然和之前的代码合并到一起，该做判断的地方，加上判断就行了

```typescript
export function track(target: object, type: TrackOpTypes, key: unknown) {
  // 暂停依赖收集开关，没有activeEffect或者shouldTrack为false时，不进行依赖收集
  if (!shouldTrack || activeEffect === undefined) { 
    return;
  }
  
  // 1.根据target从buckets中获取depsMap,它是一个Map对象，保存类型是key---effects的键值对
  let depsMap = targetMap.get(target);
  // 如果depsMap不存在,则创建一个新的Map对象
  if (!depsMap) {
    targetMap.set(target, (depsMap = new Map()));
  }

  // 2.根据key从depsMap中获取effects,它是一个Set对象
  // 里面存储的是当前key对应的副作用函数
  let deps = depsMap.get(key);
  // 如果deps不存在,则创建一个新的Set对象
  if (!deps) {
    depsMap.set(key, (deps = new Set()));
  }

  // 如果不存在activeEffect再加入
  if (!deps.has(activeEffect)) {
    // 3.将activeEffect存储到deps中
    deps.add(activeEffect);
    // deps就是一个与当前副作用函数存在关联的依赖集合
    activeEffect.deps.push(deps);
  }
  
}

```

## 三、effect副作用函数的处理

### 1、基本的TS处理

先看原来的js代码，首先参数需要的其实就是一个`fn`函数，但是这个函数是需要有一个返回值类型的，具体是什么类型不知道，那我们就可以使用泛型`()=>T`

然后是参数`options`，也就是现在的`ReactiveEffectOptions`类型

最后函数我们返回的也是`ReactiveEffect`类型的函数，所以直接`ReactiveEffect<T>`

```typescript
function effect<T = any>(
  fn: () => T,
  options: ReactiveEffectOptions = {}
): ReactiveEffect<T> {
......
}
```

剩余的部分，只需要稍作修改，直接复制粘贴进去即可。

```typescript
function effect<T = any>(
  fn: () => T,
  options: ReactiveEffectOptions = {}
): ReactiveEffect<T> {
  const effectFn:ReactiveEffect = () => {
    cleanup(effectFn);
    // 当effectFn执行时，将其设置为当前激活的副作用函数
    activeEffect = effectFn;
    // 在调用副作用函数之前，将其压入effectStack栈
    effectStack.push(effectFn);
    // 将fn的执行结果保存到res中
    const res = fn();
    // 在调用副作用函数之后，将其弹出effectStack栈
    effectStack.pop();

    // activeEffect始终指向当前effectStack栈顶的副作用函数
    activeEffect = effectStack[effectStack.length - 1];

    return res;
  }

  // 将options对象挂载到effectFn函数上
  effectFn.options = options;

  // 在effectFn函数上又挂载了deps数组，目的是在收集依赖时可以临时记录依赖关系
  // 在effectFn函数上挂载，其实就相当于在activeEffect挂载
  effectFn.deps = [];

  // 只有非lazy的情况，才会立即执行副作用函数
  if (!options.lazy) {
    effectFn();
  }

  return effectFn;
}

function cleanup(effect: ReactiveEffect) {
  const { deps } = effect;
  if (deps.length) {
    for (let i = 0; i < deps.length; i++) {
      deps[i].delete(effect);
    }
    deps.length = 0;
  }
}
```

### 2、createReactiveEffect再封装

这个函数里面其实创建了新的effect函数，我们可以再封装一下

```typescript
function createReactiveEffect<T = any>(
  fn: () => T,
  options: ReactiveEffectOptions
): ReactiveEffect<T> { 
  const effect = function reactiveEffect(): unknown {
    if (!effectStack.includes(effect)) {
      cleanup(effect);
      try {
        // 当effectFn执行时，将其设置为当前激活的副作用函数
        activeEffect = effect;
        // 在调用副作用函数之前，将其压入effectStack栈
        effectStack.push(effect);
        // 将fn的执行结果保存到res中
        const res = fn();
        return res;
      }
      finally { 
        // 在调用副作用函数之后，将其弹出effectStack栈
        effectStack.pop();
        // activeEffect始终指向当前effectStack栈顶的副作用函数
        activeEffect = effectStack[effectStack.length - 1];
      }
    }
  } as ReactiveEffect;

  // 将options对象挂载到effectFn函数上
  effect.options = options;

  // 在effectFn函数上又挂载了deps数组，目的是在收集依赖时可以临时记录依赖关系
  // 在effectFn函数上挂载，其实就相当于在activeEffect挂载
  effect.deps = [];
  return effect
}

function effect<T = any>(
  fn: () => T,
  options: ReactiveEffectOptions = {}
): ReactiveEffect<T> {
  const effect = createReactiveEffect(fn, options)
  if (!options.lazy) {
    effect()
  }
  return effect
}
```

### 3、effect函数直接嵌套的问题处理

而且，我们之前通过js解决了effect嵌套的问题，本质上解决的是如果出现了多个副作用函数嵌套，简单直观的说，是在vue里面父组件套子组件渲染的情况。这是很正常的需要。

但是，我们还需要注意一个边界问题。如果传入的直接就是一个已经被封装的effect函数呢？这样我们再去处理就没有必要了。

看一眼区别，之前处理的情况：

```typescript
effect(function effectFn1() {
  console.log("外层 effectFn1 执行");

  effect(function effectFn2() {
    console.log("内层 effectFn2 执行");
    layer2.innerHTML = proxy.age;
  });

  layer1.innerHTML = proxy.name;
})
```

我现在说的是，如果出现了，下面的情况：

```typescript
const effectFn = effect(() => {
  console.log('副作用函数');
});

effect(effectFn);
```

如果出现了下面的情况，我们将原始的内部函数也就是第一个effect里面的回调函数，拿出来进行处理就行了。

要做到这一点，其实再最开始的时候，给`ReactiveEffect`多设置两个属性：

```typescript
export interface ReactiveEffect<T = any> {
  (): T;
  deps: Array<Dep>;
  options: ReactiveEffectOptions;
  _isEffect: true;
  raw: () => T;
}
```

所以，当创建副作用函数`createReactiveEffect`的时候，处理一下这两个属性

```typescript
function createReactiveEffect<T = any>(
  fn: () => T,
  options: ReactiveEffectOptions
): ReactiveEffect<T> { 
  // ......其他代码省略

  // 如果发生了effect嵌套，直接将内部的fn给到effect.raw
  effect._isEffect = true
  effect.raw = fn;

  return effect
}
```

调用的时候，再进行处理

```typescript
export function isEffect(fn: any): fn is ReactiveEffect {
  return fn && fn._isEffect === true
}

function effect<T = any>(
  fn: () => T,
  options: ReactiveEffectOptions = {}
): ReactiveEffect<T> {
  // 如果fn已经是一个副作用函数
  if (isEffect(fn)) {
    fn = fn.raw
  }

  const effect = createReactiveEffect(fn, options)
  if (!options.lazy) {
    effect()
  }
  return effect
}
```

## 