## computed的TS改造

computed我们现在完全可以新建一个文件来单独处理他，创建`computed.ts`文件，把我们之前写好的js代码先复制粘贴进去，慢慢来填充里面的TS类型。

首先两个工具函数，我们直接放到utils.ts中，加上基本的类型注解即可

```typescript
// 判断是否为函数
export const isFunction = (val:unknown) => typeof val === "function";
// 空函数
export const NOOP = () => {};
```

接下来`computed`函数，我们需要处理**参数的类型**和**返回值的类型**。

参数类型无非两种，那么直接传递函数当做`getter`，要么传递的是一个对象，对象中有`getter/setter`

```typescript
export type ComputedGetter<T> = (ctx?: any) => T;
export type ComputedSetter<T> = (v: T) => void;

export interface WritableComputedOptions<T> {
  get: ComputedGetter<T>
  set: ComputedSetter<T>
}
```

返回值类型，我们最后返回的是一个`ComputedRefImpl`，里面有`effect`和`value`，value我们知道是Ref的显著特点，无论如何，我们可以创建Ref的类型，然后再创建只读的`Computed`类型和可写的`Computed`类型，我们可以写出下面的代码：

```typescript
export interface Ref<T = any> {
  value: T
}

export interface WritableComputedRef<T> extends Ref<T> {
  readonly effect: ReactiveEffect<T>
}

export interface ComputedRef<T = any> extends WritableComputedRef<T> {
  readonly value: T
}
```

那么`computed`函数我们通过注解可以写成下面的样子，并且为了更友好的TS提示，我们可以写成重载，不同的类型参数返回值的类型也是不同的。

```typescript
export function computed<T>(getter: ComputedGetter<T>): ComputedRef<T>
export function computed<T>(
  options: WritableComputedOptions<T>
): WritableComputedRef<T>
export function computed<T>(getterOrOptions: ComputedGetter<T> | WritableComputedOptions<T>) {
  let getter;
  let setter;

  // 如果参数只是一个函数,则getter为这个函数,setter为默认为空函数
  if (isFunction(getterOrOptions)) {
    getter = getterOrOptions;
    setter = NOOP;
  } else {
    getter = getterOrOptions.get;
    setter = getterOrOptions.set;
  }

  return new ComputedRefImpl(getter, setter);
}
```

最后就是`ComputedRefImpl`类的修改了，稍微注意TS类的处理上的问题，以前js只能用`_value`这种以`_`标识性的方式来表示私有，TS可以直接写private修饰符。

并且在构造函数中，也能够直接简写。

而且现在我们在TS环境中，也能非常方便的让`ComputedRefImpl`区分只读情况

```typescript
class ComputedRefImpl<T> {
  private _value!: T
  private _dirty = true
  public readonly effect: ReactiveEffect<T>
  public readonly [ReactiveFlags.IS_READONLY]: boolean

  constructor(
    getter: ComputedGetter<T>,
    private readonly _setter: ComputedSetter<T>,
    isReadonly: boolean
  ) {
    this._setter = _setter;
    this.effect = effect(getter, {
      lazy: true,
      scheduler: () => {
        if (!this._dirty) {
          this._dirty = true;
          trigger(toRaw(this), TriggerOpTypes.SET, "value");
        }
      },
    });

    this[ReactiveFlags.IS_READONLY] = isReadonly
  }

  get value() {
    if (this._dirty) {
      this._value = this.effect();
      this._dirty = false;
    }
    track(toRaw(this), TrackOpTypes.GET, 'value')
    return this._value;
  }

  set value(newValue) {
    this._setter(newValue);
  }
}
```

既然多加了`readonly`的处理，在`computed`调用的时，加上boolean值即可

```typescript
export function computed<T>(getterOrOptions: ComputedGetter<T> | WritableComputedOptions<T>) {
  //......其他省略

  return new ComputedRefImpl(
    getter,
    setter,
    isFunction(getterOrOptions) || !getterOrOptions.set
  );
}
```
