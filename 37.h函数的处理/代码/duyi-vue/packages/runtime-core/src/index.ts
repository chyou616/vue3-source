export {
  createRenderer,
  options,
  shouldSetAsProps,
  normalizeClass,
  lis,
} from "./renderer";
export { watch } from "./apiWatch";
export { h } from "./h";
