## 组件的基本实现原理

我们其实知道，组件就是一个对象，一个最基本的组件，类似于下面的形式

```typescript
const MyComponent = {
  name: 'MyComponent',
  data(){
    return {
      foo:'hello world'
    }
  },
  render(){
    return {
      type:'div',
      children: '我是文本内容'
    }
  }
}
```

我们知道，`data()`函数中其实就是响应式数据，`render()`函数就是组件要渲染的内容。

当然这只是组件对象，我们还需要虚拟DOM进行调用：

```typescript
const CompVNode = {
  type: MyComponent
}
```

可以看到，我们现在的虚拟DOM，`type`就是**组件对象**，也就是现在我们需要挂载组件的时候，根据对象类型去进行处理

根据这个形式，其实我们就能写出相关组件的处理代码：

```typescript
export function createRenderer<
  HostNode = any,
  HostElement = any
  >(options: RendererOptions<HostNode, HostElement>) {
	//其他省略......
  function patch(oldVNode: any, newVNode:any, container:any, anchor = null) {
    // 其他省略
    else if (typeof type === "object") {
      if (!oldVnode) {
        // 挂载组件
        mountComponent(newVnode, container, anchor);
      }
      else { 
        // 更新组件
      }
    }
  }
  
	function mountComponent(vnode:any, container:any, anchor:any) {
    // 通过vnode获取组件的选项对象
    const componentOptions = vnode.type;
    // 获取组件的渲染函数
    const { render } = componentOptions;
    // 执行渲染函数，获取要渲染的内容，其实也就是函数返回的虚拟DOM对象
    const subTree = render();
    // 挂载
    patch(null, subTree, container, anchor);
  }
}

```

这样，其实就和之前挂载虚拟DOM没有什么区别了

```typescript
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h} = VueRuntimeCore
  const MyComponent = {
    name: 'MyComponent',
    data(){
      return {
        foo:'hello world'
      }
    },
    render(){
      return {
        type:'div',
        children: '我是文本内容'
      }
    }
  }

  // 虚拟DOM
  // const CompVNode = {
  //   type: MyComponent
  // }
  render(h(MyComponent),document.getElementById('app'))
</script>
```

上面导出的render函数，是将之前的createRenderer函数稍微封装了一下

```typescript
function ensureRenderer() { 
  return createRenderer(options);
}

export const render = ensureRenderer().render;
```

在`index.ts`中，导出`render`函数即可

当然，这只是渲染函数，如果我希望使用数据，并且还希望是响应式数据，比如在调用的使用写成下面的样子

```typescript
const MyComponent = {
  name: 'MyComponent',
  data(){
    return {
      foo:'hello world'
    }
  },
  render(){
    return {
      type:'div',
      children: '我是文本内容--->' + this.foo
    }
  }
}
```

那我们就需要对`data()`函数再进行处理

```typescript
function mountComponent(vnode:any, container:any, anchor:any) {
  // 通过vnode获取组件的选项对象
  const componentOptions = vnode.type;
  // 获取组件的渲染函数
  const { render, data } = componentOptions;

  // 调用data获取原始数据，并用reactive进行响应式处理
  const state = reactive(data());

  // 调用render函数时，将this设置为state
  // 这样在render函数中就可以通过this访问到state
  const subTree = render.call(state, state);
  // 挂载
  patch(null, subTree, container, anchor);
}
```

上面的代码主要是两个步骤：

- 通过组件对象获取data()函数并执行，然后调用reactive函数将data()函数返回的状态包装为响应式数据
- 在调用render函数的时候，将其this的指向设置为响应式数据state，同时将state作为render函数的第一个参数传递

这样，就实现了对组件自身状态的支持，而且我们还能在渲染函数内访问组件自身的状态。

当然，当组件发生变化的时候需要更新，其实我们只需要将渲染任务包装到effect函数中就可以了。

```typescript
function mountComponent(vnode:any, container:any, anchor:any) {
  // 通过vnode获取组件的选项对象
  const componentOptions = vnode.type;
  // 获取组件的渲染函数
  const { render, data } = componentOptions;

  // 调用data获取原始数据，并用reactive进行响应式处理
  const state = reactive(data());

  // 将render函数包装到effect函数中
  effect(() => { 
    // 调用render函数时，将this设置为state
    // 这样在render函数中就可以通过this访问到state
    const subTree = render.call(state, state);
    // 挂载
    patch(null, subTree, container, anchor);
  })
}
```

这样，一旦组件自身的响应式数据发生变化，组件就会自动的重新执行渲染函数(当然现在仅仅只能重新再进行挂载)。

不过这里要注意一个问题，effect的执行是同步的，因此当响应式数据发生变化的时候，与之关联的副作用函数就会同步执行。也就是说，如果多次修改响应式数据的值，就会导致渲染函数执行多次，这实际上是没有必要的。我们需要的是无论响应式数据短时间内修改多少次，副作用函数只会重新执行一次就行了。

这个其实我们之前在讲调度器的时候已经实现了，现在我们拿过来，稍微修改一下就可以了。

其实基本原理就是当副作用函数需要重新执行的时候，我们不会立即执行，而是将其缓冲到一个微任务队列中，等到执行栈清空之后，再将它从微任务队列中取出来执行。

```typescript
function mountComponent(vnode:any, container:any, anchor:any) {
  // 通过vnode获取组件的选项对象
  const componentOptions = vnode.type;
  // 获取组件的渲染函数
  const { render, data } = componentOptions;

  // 调用data获取原始数据，并用reactive进行响应式处理
  const state = reactive(data());

  // 将render函数包装到effect函数中
  effect(() => { 
    // 调用render函数时，将this设置为state
    // 这样在render函数中就可以通过this访问到state
    const subTree = render.call(state, state);
    // 挂载
    patch(null, subTree, container, anchor);
  }, {
    scheduler: queueJob
  })
}


// 自定义调度器
let isFlushing = false;
// 用set集合来存储副作用函数，使用Set是因为Set不允许有重复的值，
// 这样可以保证同一个副作用函数不会重复被添加到队列中
const queue = new Set();

const p = Promise.resolve();

function queueJob(job:any) {
  queue.add(job);
  if (!isFlushing) { 
    // 设置为true，避免重复刷新
    isFlushing = true;

    p.then(() => { 
      queue.forEach((job:any) => job());
    })
    .finally(() => { 
      isFlushing = false;
      queue.clear();
    })
  }
}
```

界面测试：

```typescript
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h} = VueRuntimeCore

  // const MyComponent = {
  //   name: 'MyComponent',
  //   data(){
  //     return {
  //       foo:'hello world'
  //     }
  //   },
  //   render(){
  //     return {
  //       type:'div',
  //       children: [
  //         {
  //           type:'span',
  //           children: '我是文本内容--->' + this.foo
  //         },
  //         {
  //           type:'button',
  //           props:{
  //             onClick:()=>{
  //               console.log('click')
  //               this.foo = 'hello vue'
  //             }
  //           },
  //           children: '按钮'
  //         }
  //       ]
  //     }
  //   }
  // }

	const MyComponent = {
    name: 'MyComponent',
    data(){
      return {
        foo:'hello world'
      }
    },
    render(){
      return h("div",{},[
        h("span",{},'我是文本内容--->' + this.foo),
        h("button",{
          onClick:()=>{
            console.log('click')
            this.foo = 'hello vue'
          }
        },'按钮')
      ])
    }
  }

  render(h(MyComponent),document.getElementById('app'))
</script>
</html>
```

当然，我们现在每次重新渲染的patch函数，第一个参数都是null，也就是每次都在重新挂载，这肯定是不完整的，完整的应该是，每次更新的时候，都拿新的subTree与上一次组件所渲染的subTree进行更新。

为此，我们最好实现一个组件实例，用它来维护组件的整个生命周期状态。这样渲染器才能在正确的时机执行合适的操作。