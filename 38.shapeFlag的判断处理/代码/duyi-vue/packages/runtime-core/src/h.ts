import { isString, ShapeFlags,isArray, isObject } from '@vue/shared'

function isVNode(value:any) { 
  return value?.__v_isVNode
}

function createVNode(type: any, props: any, children: any) {
  const shapeFlag = isString(type) ? ShapeFlags.ELEMENT : 0

  const vnode = {
    __v_isVNode: true,
    type,
    props,
    children,
    component: null,
    el: null,
    key: props?.key,
    shapeFlag
  }

  if (children) { 
    if (isArray(children)) {
      vnode.shapeFlag |= ShapeFlags.ARRAY_CHILDREN
    }
    else { 
      vnode.shapeFlag |= ShapeFlags.TEXT_CHILDREN
    }
  }

  return vnode;
}

export function h(type: any, props: any, children: any) { 
  let l = arguments.length
  if (l === 2) {
    if (isObject(props) && !isArray(props)) {
      // 判断是不是虚拟节点
      if (isVNode(props)) {
        return createVNode(type, null, [props])
      }
      // 如果不是虚拟节点，就是props属性
      return createVNode(type, props, null)
    }
    else { 
      return createVNode(type, null, props)
    }
  }
  else { 
    if (l > 3) { 
      // 如果参数大于3个，除了前面的参数，后面的参数都视为children，直接转换为数组
      children = Array.prototype.slice.call(arguments, 2)
    }
    else if(l === 3 && isVNode(children)) { 
      children = [children]
    }

    return createVNode(type, props, children)
  }
}