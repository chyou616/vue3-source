## 模板AST的转换设计

为了更好了查看模板AST的对象结构，我们创建一个专门了函数，便于查看树形的结构，我们直接进行深度优先的遍历即可

```typescript
function dump(node, indent = 0) {
  const type = node.type;
  // 节点描述，如果是根节点，没有描述
  // 如果是Element类型的节点，使用node.tag作为节点的描述
  // 如果是Text类型的节点，使用node.content作为节点的描述
  const desc =
    node.type === "Root"
      ? ""
      : node.type === "Element"
      ? node.tag
      : node.content;

  // 打印节点类型和描述信息
  console.log(`${"-".repeat(indent)}${type}: ${desc}`);

  if (node.children) {
    node.children.forEach((n) => dump(n, indent + 3));
  }
}
```

运行之前代码

```typescript
const ast = parse(template);
dump(ast);
```

可以得到下面的结果：

```typescript
Root: 
---Element: div
------Element: p
---------Text: VueVue
------Element: p
---------Text: TemplateTemplate
```

那其实，如果我们要访问模板AST，使用一样的方式去处理就可以了

```typescript
function traverseNode(ast) {
  const currentNode = ast;
	// 如果有子节点，递归的调用traverseNode函数进行遍历
  const children = currentNode.children;
  if (children) {
    for (let i = 0; i < children.length; i++) {
      traverseNode(children[i]);
    }
  }
}
```

有了这个基本操作之后，我们就可以在这个函数的基础之上，再做变化。

比如，我们要对当前节点进行操作，直接判断修改一下即可。

```typescript
function traverseNode(ast) {
  const currentNode = ast;
	// 如果有子节点，递归的调用traverseNode函数进行遍历
  const children = currentNode.children;
  
  // 对当前节点进行操作
  if(currentNode.type === 'Element' && currentNode.tag === 'p'){
    // 将所有p标签转换为h1标签
    currentNode.tag = 'h1'
  }
  
  if (children) {
    for (let i = 0; i < children.length; i++) {
      traverseNode(children[i]);
    }
  }
}
```

修改之后，我们封装成transform函数，并且通过dump函数打印一下：

```typescript
function transform(ast) {
  traverseNode(ast);
  // 打印 AST 信息
  dump(ast);
}
```

这样，就得到下面的代码：

```typescript
Root: 
---Element: div
------Element: h1
---------Text: Vue
------Element: h1
---------Text: Template
```

如果还要进行转换，比如将文本内容进行重复，那一样的道理，直接在`traverseNode`函数中，再添加相应的内容就行了。

```typescript
function traverseNode(ast) {
  const currentNode = ast;

  // 对当前节点进行操作
  if(currentNode.type === 'Element' && currentNode.tag === 'p'){
    // 将所有p标签转换为h1标签
    currentNode.tag = 'h1'
  }
  
  // 如果节点类型是Text
  if(currentNode.type === 'Text'){
    // 内容简单重复3次
    currentNode.content = currentNode.content.repeat(3)
  }

  const children = currentNode.children;
  if (children) {
    for (let i = 0; i < children.length; i++) {
      traverseNode(children[i]);
    }
  }
}
```

我们当然不能这样无限制的去进行硬编码。

首先这里无论是对Element节点的操作还是对Text的操作，我们都可以封装到函数中，既然可以封装到函数中，我们就可以使用回调函数去进行处理，到时候，通过参数传入进来即可。

其次，我们也可以像之前虚拟节点的处理一样，将我们需要的一些关键信息进行记录，比如当前的节点是什么，当前节点的父节点是什么，或者当前节点在父节点中的位置索引等等，这些我们都可以进行记录。

所以，我们可以将这些信息封装在一个对象中，然后进行传入即可。比如：

```typescript
function transformElement(node) {
  if (node.type === "Element" && node.tag === "p") {
    node.tag = "h1";
  }
}

function transformText(node) {
  if (node.type === "Text") {
    node.content = node.content.repeat(3);
  }
}

function transform(ast) {
  const context = {
    // 当前正在转换的节点
    currentNode: null,
    // 当前转换节点的父节点
    parent: null,
    // 当前节点在父节点children中的位置索引
    childIndex:0,
    nodeTransforms: [
      transformElement,
      transformText
    ]
  }
  // 调用 traverseNode 完成转换
  traverseNode(ast,context);
  // 打印 AST 信息
  dump(ast);
}
```

相应的，我们在`traverseNode`函数中做出处理即可。

```typescript
function traverseNode(ast, context) {
  // 设置当前转换的节点信息context.currentNode
  context.currentNode = ast

  // nodeTransforms是一个数组，每个元素都是一个函数
  const transforms = context.nodeTransforms
  for (let i = 0; i < transforms.length; i++) {
    transforms[i](context.currentNode, context)
  }

  const children = context.currentNode.children
  if (children) {
    for (let i = 0; i < children.length; i++) {
      // 递归调用之前，将当前节点设置为父节点
      context.parent = context.currentNode
      // 设置位置索引
      context.childIndex = i
      traverseNode(children[i], context)
    }
  }
}
```

有了context来记录信息之后，我们就可以实现一些其他功能了，比如节点替换功能，在对AST进行转换的时候，我们可能希望把某些节点替换为其他类型的几点，例如，将所有文本节点替换为一个元素节点。

```typescript
function transform(ast) {
  const context = {
    currentNode: null,
    parent: null,
    childIndex:0,
    replaceNode(node){
      // 找到当前节点在父节点children中的位置，也就是上面记录的childIndex
      // 然后使用新节点替换
      context.parent.children[context.childIndex] = node
      // 由于当前节点已经被替换，所以需要将当前节点currentNode指向新节点
      context.currentNode = node
    },
    nodeTransforms: [
      transformElement,
      transformText
    ]
  }
  // 调用 traverseNode 完成转换
  traverseNode(ast,context);
  // 打印 AST 信息
  dump(ast);
}
```

这样，之前的代码我们就可以直接使用context上下文的函数进行处理

```typescript
function transformText(node, context) {
  if (node.type === "Text") {
    // context.replaceNode({
    //   type: "Text",
    //   content: node.content.repeat(3)
    // })

    // 直接替换节点也可以
    context.replaceNode({
      type: "Element",
      tag: 'span'
    })

  }
}
```

同样，如果希望移除当前访问的节点，再次添加类似的代码即可，比如`context.removeNode`

```diff
function transform(ast) {
  const context = {
    currentNode: null,
    parent: null,
    childIndex:0,
    replaceNode(node){
      // 找到当前节点在父节点children中的位置，也就是上面记录的childIndex
      // 然后使用新节点替换
      context.parent.children[context.childIndex] = node
      // 由于当前节点已经被替换，所以需要将当前节点currentNode指向新节点
      context.currentNode = node
    },
+    removeNode(){
+      if(context.parent){
+        // 从父节点的children中删除当前节点
+        context.parent.children.splice(context.childIndex,1)
+        // 当前节点指向null
+        context.currentNode = null
+      }
    },
    nodeTransforms: [
      transformElement,
      transformText
    ]
  }
  // 调用 traverseNode 完成转换
  traverseNode(ast,context);
  // 打印 AST 信息
  dump(ast);
}
```

同样，处理函数可以修改为：

```typescript
function transformText(node, context) {
  if (node.type === "Text") {
    // context.replaceNode({
    //   type: "Text",
    //   content: node.content.repeat(3)
    // })

    // 直接替换节点也可以额
    // context.replaceNode({
    //   type: "Element",
    //   tag: 'span'
    // })

    // 如果是文本节点，直接调用removeNode删除
    context.removeNode()
  }
}
```

由于这里删除了节点，所以在之前的`transformNode`中需要做一下判断处理

```diff
function traverseNode(ast, context) {
  // 设置当前转换的节点信息context.currentNode
  context.currentNode = ast

  // nodeTransforms是一个数组，每个元素都是一个函数
  const transforms = context.nodeTransforms
  for (let i = 0; i < transforms.length; i++) {
    transforms[i](context.currentNode, context)
+    if(!context.currentNode){
+      // 如果当前节点为null，说明节点已经被删除，直接返回
+      return
+    }
  }
  //...其他省略
}
```

### 总结：

其实，回过头来看，就传入一个简单的`context`对象。

但是通过`currentNode`，`parent`等属性达到了**存储全局编译状态**的效果，这样就**便于转换函数访问和修改节点**。

还有`nodeTransforms` 是一个数组，其中的每个元素都是一个函数，当然我们现在的函数写的很简单，但是**这些函数我们可以用于定义对模板 AST 节点的各种转换规则**，这样，实质上形成了编译器的 **节点转换插件**。

实际上，Vue 内置的一些转换逻辑（如处理 `v-if`、`v-for`、组件节点等）也以插件的形式存在于 `nodeTransforms` 中，`nodeTransforms` 允许开发者通过注册自定义的转换函数来扩展模板编译器。例如，添加自定义指令或优化规则

而且设计成数组，也更符合模块化设计，不同类型的 AST 节点可以由不同的转换函数处理。通过将这些逻辑分散到多个函数中，可以让代码更易于理解和维护，例如，`transformIf（vIf）` 处理条件渲染节点，`transformFor（vFor）` 处理循环节点

并且`traverseNode`函数是深度优先遍历的，每个节点都会依次调用 `nodeTransforms` 中的所有函数，通过这种方式，可以确保父节点的逻辑能够在子节点处理完成后执行

## 进入与退出

我们的代码其实非常简单的写了一下`transformElement`函数，实际上这个函数主要作用是对模板中的元素节点进行转换，并生成相应的抽象语法树（AST）节点，供后续的代码生成阶段使用。

在转换AST节点的过程中，往往需要根据其子节点的情况来决定如何对当前节点进行转换，这就要求父节点的转换操作必须等待其所有子节点全部转换完毕之后再执行。

但是我们现在设计的转换流程并不支持这个能力，我们是从根节点开始，顺序往下执行的流程，如下图：

![image-20241223144619792](./assets/image-20241223144619792.png)

当一个节点被处理时，意味着它的父节点已经被处理完毕了，我们无法再回头重新处理父节点。

更加理想的转换流程应该像下面这样：

![image-20241223144812409](./assets/image-20241223144812409.png)

对节点的访问，其实可以分为两个阶段，**进入和退出阶段**，也可以称之为**前置转换阶段（Pre-Transform）和后置转换阶段（Post-Transform）**。

当转换函数处于进入阶段时，它会先进入父节点，再进入子节点。而当转换函数处于退出阶段时，会先退出子节点，再退出父节点。这样我们在退出节点阶段对当前访问的节点进行处理，就能保证子节点全部处理完毕。

比如下面的伪代码：

```typescript
<MyComponent>
  <template v-slot:header>Header Content</template>
  <template v-slot:default>Default Content</template>
</MyComponent>
```

在这个例子中：

- 在进入阶段，会识别这是一个组件节点，并初始化一些基础信息。
- 但是，插槽内容需要等所有子节点（插槽模板）都处理完成后才能生成，这部分逻辑父节点就需要等待所有子节点全部处理完成才能执行。

为了能够处理这种情况，我们可以让**转换函数返回一个回调函数**，在`traverseNode`函数中增加一个数组，用来**存储**转换函数返回的回调函数，并且在`traverseNode`函数的最后，执行缓存在数组中的函数。这样就保证了，当**退出阶段的回调函数执行时，当前访问节点的子节点已经全部被处理过了**

有了这样的设计之后，我们在编写转换函数的时候，可以将转换逻辑卸载退出阶段的回调函数中，从而保证在对当前访问的节点进行转换之前，其子节点一定全部处理完毕了。

```typescript
function transformElement(node) {
  // console.log(`进入transformElement：${JSON.stringify(node)}`)

  // return () => {
  //   console.log(`退出transformElement：${JSON.stringify(node)}`)
  // }
}

function transformText(node, context) {
  console.log(`进入transformText：${JSON.stringify(node)}`)

  return () => {
    console.log(`退出transformText：${JSON.stringify(node)}`)
  }
}
```

**traverseNode函数**

```diff
function traverseNode(ast, context) {
  // 设置当前转换的节点信息context.currentNode
  context.currentNode = ast

  // 增加退出阶段的回调函数数组
+  const exitFns = [];

  // nodeTransforms是一个数组，每个元素都是一个函数
  const transforms = context.nodeTransforms
  for (let i = 0; i < transforms.length; i++) {
+    const onExit = transforms[i](context.currentNode, context)
+    if(onExit){
+      exitFns.push(onExit)
+    }

    if(!context.currentNode){
      // 如果当前节点为null，说明节点已经被删除，直接返回
      return
    }
  }

  const children = context.currentNode.children
  if (children) {
    for (let i = 0; i < children.length; i++) {
      // 递归调用之前，将当前节点设置为父节点
      context.parent = context.currentNode
      // 设置位置索引
      context.childIndex = i
      traverseNode(children[i], context)
    }
  }

+  // 在节点处理的最后阶段，执行所有的退出函数
+  let i = exitFns.length
+  while(i--){
+    exitFns[i]()
+  }
}
```

