#### lazy

我们首先可以通过lazy字段，简单的空effect函数进行懒执行操作，其实还是通过options字段，传递对应的lazy字段，然后在effect进行判断即可。

```typescript
const effectFn = effect(
  () => { 
    return proxy.age + 10;
  },
  {
    lazy:true
  }
)
```

```diff
function effect(fn, options={}) {
  const effectFn = () => {
    cleanup(effectFn);
    // 当effectFn执行时，将其设置为当前激活的副作用函数
    activeEffect = effectFn;

    // 在调用副作用函数之前，将其压入effectStack栈
    effectStack.push(effectFn);
    fn();

    // 在调用副作用函数之后，将其弹出effectStack栈
    effectStack.pop();

    // activeEffect始终指向当前effectStack栈顶的副作用函数
    activeEffect = effectStack[effectStack.length - 1];
  };
  // 将options对象挂载到effectFn函数上
  effectFn.options = options;

  // 在effectFn函数上又挂载了deps数组，目的是在收集依赖时可以临时记录依赖关系
  // 在effectFn函数上挂载，其实就相当于在activeEffect挂载
  effectFn.deps = [];

+  // 只有非lazy的情况，才会立即执行副作用函数
+  if(!options.lazy) {
+    // 执行副作用函数
+    effectFn();
+  }
  
+  // 将副作用函数作为返回值返回
+  return effectFn;
}
```

最后手动调用一下

```typescript
const effectFn = effect(
  () => { 
    return proxy.age + 10;
  },
  {
    lazy:true
  }
)
effectFn();
```

当然，这样手动的去调用肯定不是我们想要的效果。

#### 返回值

想想我们在vue3中怎么去使用computed计算属性的？我们传入的可能是一个**回调函数**（`getter`）或者直接就是**访问器属性**`get/set`，无论如何**最后返回的是一个值**，因此，我们稍微改动一下`effect`函数的处理

```diff
function effect(fn, options={}) {
  const effectFn = () => {
    cleanup(effectFn);
    // 当effectFn执行时，将其设置为当前激活的副作用函数
    activeEffect = effectFn;

    // 在调用副作用函数之前，将其压入effectStack栈
    effectStack.push(effectFn);
    
+ 	 // 将fn的执行结果保存到res中
+    const res = fn();

    // 在调用副作用函数之后，将其弹出effectStack栈
    effectStack.pop();

    // activeEffect始终指向当前effectStack栈顶的副作用函数
    activeEffect = effectStack[effectStack.length - 1];
    
+		// 将res作为effectFn的返回值
+    return res;
  };
  // 将options对象挂载到effectFn函数上
  effectFn.options = options;

  // 在effectFn函数上又挂载了deps数组，目的是在收集依赖时可以临时记录依赖关系
  // 在effectFn函数上挂载，其实就相当于在activeEffect挂载
  effectFn.deps = [];

  // 只有非lazy的情况，才会立即执行副作用函数
  if(!options.lazy) {
    // 执行副作用函数
    effectFn();
  }
  
  // 将副作用函数作为返回值返回
  return effectFn;
}
```

这样，当我们运行之前的函数，至少可以得到一个值：

```typescript
const effectFn = effect(
  () => { 
    return proxy.age + 10;
  },
  {
    lazy:true
  }
)

const value = effectFn();

console.log(value)
```

#### computed函数封装

根据这里effect的处理，那我们就可以来实现`computed`计算属性。其实最简单的，就是将我们上面的获取effectFn的相关代码封装成一个函数就行了。

```typescript
function computed(getterOrOptions) { 
  const effectFn = effect(getterOrOptions, { lazy: true });
  
  const obj = {
    get value() {
      console.log("---obj.value---")
      return effectFn();
    }
  }

  return obj;
}

const res = computed(() => proxy.age + 10);
console.log(res.value);
```

现在当然还没有做任何的处理，如果我们多次打印`res.value`，当然还是会多次执行`effectFn`函数。这当然需求修改。

#### dirty缓存值

现在无非就是要缓存上一次已经执行的值，这个是闭包处理经常干的事情，比如：

```typescript
function lazyInit(){
  let value;
  return function(){
    if(value === undefined){
      value = ComplexCalc();
    }
    return value
  }
}

// 复杂耗时计算
function ComplexCalc(){
  let sum = 0;
  for(let i=0; i<=1000; i++){
    sum += i;
  }
  return sum
}

const getValue = lazyInit()

console.log(getValue())
console.log(getValue())
```

那我们这里用一个标识`dirty`进行处理就行了。

```typescript
function computed(getterOrOptions) { 
  let value;
  // dirty标志，用来标识是否需要重新计算，初始值为true，表示需要重新计算
  let dirty = true;

  const effectFn = effect(getterOrOptions, { lazy: true });
  
  const obj = {
    get value() {
      // 如果dirty为true,则重新计算value的值
      if (dirty) { 
        console.log("---obj.value---")
        value = effectFn();
        // 计算完成后，将dirty设置为false,下一次直接使用缓存的value值
        dirty = false;
      }
      return value;
    }
  }

  return obj;
}

const res = computed(() => proxy.age + 10);

console.log(res.value);
console.log(res.value);
console.log(res.value);
```

打印：

```text
---obj.value---
28
28
28
```

#### 修改dirty的处理

但是现在还是有问题的，当我们修改了`proxy.age`之后，`computed`由于已经进入了一次了，导致`dirty`现在为`false`，不会重新进行计算

```typescript
// 其他省略

const res = computed(() => proxy.age + 10);

console.log(res.value);
proxy.age++;
console.log(res.value);
```

打印：

```text
28
28
```

修改的办法**其实就是当属性值发生变化的时候，将`dirty`的值重置为`true`就行了**。

现在的关键点就是怎么去重置，`res.value`现在会触发`get`，而在`get`中调用了`effectFn`函数，其实只需要在`effectFn`中，传入第二个参数`scheduler`中再对`dirty`进行处理就行了

```diff
function computed(getterOrOptions) {
  let value;
  // dirty标志，用来标识是否需要重新计算，初始值为true，表示需要重新计算
  let dirty = true;

  const effectFn = effect(getterOrOptions, {
    lazy: true,
+    // 添加调度器，在调度器中将dirty设置为true
+    scheduler: () => {
+      dirty = true;
+    },
  });

  const obj = {
    get value() {
      // 如果dirty为true,则重新计算value的值
      if (dirty) {
        console.log("---obj.value---");
        value = effectFn();
        // 计算完成后，将dirty设置为false,下一次直接使用缓存的value值
        dirty = false;
      }
      return value;
    },
  };

  return obj;
}

const res = computed(() => proxy.age + 10);

console.log(res.value);
proxy.age++;
console.log(res.value);
```

#### 