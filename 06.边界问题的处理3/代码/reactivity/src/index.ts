// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2
// }

// const r = reactive(obj);
// console.log(r.a);

// r.a = 10;


// 如果代理的是同一个对象的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2
// }

// const state1 = reactive(obj);
// const state2 = reactive(obj);

// console.log(state1 === state2);


// 如果对代理对象再进行代理的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2
// }

// const state1 = reactive(obj);
// const state2 = reactive(state1);

// console.log(state1 === state2);



// 访问器属性边界问题的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2,
//   get c() { 
//     console.log('get c', this);
//     return this.a + this.b;
//   }
// }

// const state1 = reactive(obj);

// function fn() { 
//   state1.c;
// }

// fn();


// 对象的嵌套问题的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2,
//   c: {
//     d: 3
//   }
// }

// const state1 = reactive(obj);

// function fn() { 
//   state1.c.d;
// }

// fn();

// console.log(state1)
// console.log(state1.c)


// 判断对象是否有某个属性问题的处理
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2,
//   c: {
//     d: 3
//   }
// }

// const state1 = reactive(obj);

// function fn() { 
//   console.log('a' in state1);
// }

// fn();


/*
// 相关动作的处理思考
import { reactive } from "./reactive";

const obj = {
  a: 1,
  b: 2,
  c: {
    d: 3
  }
}

// const state1 = reactive(obj);

// function fn() { 
//   'a' in state1;
// }

// // 思考：之前存在a属性，这里更新改变了a的值，会对'a' in state1的结果产生影响吗？
// state1.a = 123;

// fn();


// function fn() { 
//   'e' in state1;
// }

// // 思考：之前不存在e属性，这里更新改变了e的值，会对'e' in state1的结果产生影响吗？
// (state1 as any).e = 123;

// fn();
*/


// 循环迭代的问题
// import { reactive } from "./reactive";

// const obj = {
//   a: 1,
//   b: 2,
//   c: {
//     d: 3
//   }
// }

// const state1 = reactive(obj);

// function fn() { 
//   for (const key in state1) { 
//     console.log(key)
//   }
// }
// fn();
// function fn() { 
//   Object.keys(state1);
// }


// 删除属性的问题
import { reactive } from "./reactive";

const obj = {
  a: 1,
  b: 2,
  c: {
    d: 3
  }
}

const state1 = reactive(obj);

function fn() { 
  Object.keys(state1);
}
fn();

state1.a = 2;
//@ts-ignore
state1.e = 2;

//@ts-ignore
delete state1.a;

//@ts-ignore
delete state1.f;