const options = {
  createElement(tag) { 
    return document.createElement(tag)
  },
  setElementText(el, text) {
    el.textContent = text
  },
  insert(el, parent, anchor = null) { 
    parent.insertBefore(el, anchor)
  }
}


function createRenderer(options) { 

  const { 
    createElement,
    setElementText,
    insert
  } = options

  function render(vnode, container) { 
    if (vnode) {
      patch(container._vnode, vnode, container)
    }
    else { 
      if (container._vnode) { 
        // 旧的Vnode存在，但是新的vnode不存在，说明要进行卸载操作
        // 现在简单的将container中的内容清空
        container.innerHTML = "";
      }
    }

    container._vnode = vnode;
    
  }

  function patch(oldVnode, newVnode, container) {
    // 如果旧节点不存在，就意味着是挂载，调用mountElement函数完成挂载
    if (!oldVnode) {
      mountElement(newVnode, container)
    }
    else { 
      // 如果oldVnode存在，就是更新, 暂时省略...
    }
  }

  function mountElement(vnode, container) { 
    // 创建元素
    const el = createElement(vnode.type)
    if(typeof vnode.children === 'string'){
      // 如果子节点是字符串，说明是文本节点，直接挂载
      setElementText(el, vnode.children)
    }

    insert(el, container)
  }

  // 其他可能会用到的函数...
  return {
    render
    //...其他的函数
  }
}

const vnode = {
  type: "h1",
  children: "hello world"
}

const renderer = createRenderer(options);
// 首次渲染
renderer.render(vnode, document.getElementById('app'))

// // 第二次渲染
// renderer.render(newVnode, document.getElementById('app'))

// // 第三次渲染
// renderer.render(null, document.getElementById('app'))

