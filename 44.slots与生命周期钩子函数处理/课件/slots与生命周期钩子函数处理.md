## slots处理

MyComponent组件

```typescript
<template>
	<header>
  	<slot name="header" />
  </header>
	<main>
		<slot name="main" />
	</main>
	<footer>
		<slot name="footer" />
  </footer>
</template>
```

父组件中使用`<MyComponent />`组件时,可以根据插槽名字，插入自定义的内容

```typescript
<MyComponent>
	<template #header>
    <h1>我是标题</h1>
	</template>
	<template #main>
		<p>我是内容</p>
	</template>
	<template #footer>
		<h2>我是底部</h2>
	</template>
</MyComponent>
```

这里面的原理十分简单，其实就是父组件模板中的插槽内容，被编译成了插槽函数，比如`header()`,`main()`,`footer()`等等，而插槽函数的返回值，其实就是具体的插槽内容，也就是节点的虚拟DOM，而这些函数，都放入到了插槽对象slots中。

相当于`MyComponent`组件模板，调用了slots对象对应的函数，比如：`this.$slots.header()`，由这个函数生成了`MyComponent`组件中header节点的渲染内容

```typescript
const MyComponent = {
  name:'MyComponent',
  setup(){},
  render(){
    return h("div",{},[
      h("header",{},[this.$slots.header()]),
      h("main",{},[this.$slots.main()]),
      h("footer",{},[this.$slots.footer()])
    ])
  }
}

const BaseComp = {
  name:'BaseComp',
  setup(){},
  render(){
    return h(MyComponent,{},{
      header(){
        return h("h1",{},'我是标题')
      },
      main(){
        return h("p",{},'我是内容')
      },
      footer(){
        return h("h2",{},'我是底部')
      } 
    })
  }
}
```

所以，根据这个理念，要求修改源代码的内容就比较简单了，只需要将slots对象进行绑定就行了

```typescript
function mountComponent(vnode:any, container:any, anchor:any) {
  //...... 省略代码

  const slots = vnode.children || {};

  // 定义组件实例，本质就是一个对象，用来存储与组件相关的状态
  const instance = {
    state,
    props:shallowReactive(props),
    isMounted: false,
    subTree: null,
    emit,
    slots
  }

  const setupContext = { attrs, emit, slots };

  // ......省略代码

  const renderContext = new Proxy(instance, {
    get(target, key, receiver) {
      const { state, props, emit, slots } = target;
      if (key === '$emit') return emit
      if (key === '$slots') return slots

      //......
    },
    // ......
  })
}
```

## 生命周期钩子函数

大家知道，我们在vue3中，引入[生命周期钩子函数](https://cn.vuejs.org/guide/essentials/lifecycle.html)，是通过下面的形式：

```typescript
<script setup>
import { onMounted } from 'vue'

onMounted(() => {
  console.log(`the component is now mounted1.`)
})

onMounted(() => {
  console.log(`the component is now mounted2.`)
})
</script>
```

这其实说明几个问题：

- 生命周期钩子函数是可以单独导出的
- 生命周期钩子函数是可以放在setup函数中的
- 生命周期钩子函数是可以多次被调用的
- 生命周期钩子函数应该是属于每个组件对象实例的

虽然我们之前也简单实现了生命周期钩子函数的处理，对于现在的情况，我们肯定需要再进行加强。

首先我们应该有对应的，可以单独导出的生命周期钩子函数，比如：

```typescript
// renderer.ts
export function onMounted(fn:Function) {

}

// index.ts
export {onMounted} from "./renderer";
```

其次，生命周期钩子函数应该和每个组件实例挂钩，并且可以多次执行，那么在组件实例上，就应该有对应的生命周期钩子函数的属性，并且应该是一个数组

```diff
const instance = {
  state,
  props:shallowReactive(props),
  isMounted: false,
  subTree: null,
  emit,
  slots,
+  mounted:[]
}
```

当然，生命周期钩子函数应该和组件相对应，也就是A组件的setup函数中调用`onMounted`函数会将钩子函数注册到A组件上，B组件的setup函数中调用`onMounted`函数会将钩子函数注册到B组件上。

那我们可以维护一个全局变量`currentInstance`，用它来存储当前组件实例，每当初始化组件并且执行组件的setup函数之前，先将`currentInstance`设置为当前组件，然后，当我们调用onMounted组件的时候，直接从currentInstance对象上获取函数

```typescript
let currentInstance:any = null
function setCurrentInstance(instance:any) {
  const prev = currentInstance
  currentInstance = instance
  return prev
}

export function onMounted(fn:Function) {
  if (currentInstance) {
    currentInstance.mounted.push(fn)
  }
}
```



```diff
function mountComponent(vnode:any, container:any, anchor:any) {
  //......省略
  const instance = {
    state,
    props:shallowReactive(props),
    isMounted: false,
    subTree: null,
    emit,
    slots,
+    mounted:[]
  }


  // 存储setup函数返回的数据
  let setupState = null;
  if (setup) { 
    const setupContext = { attrs, emit, slots };

+    // 调用setup函数之前，设置当前组件实例
+    const prevInstance = setCurrentInstance(instance)

    const setupResult = setup(shallowReadonly(instance.props), setupContext);

+    // 调用setup函数之后，恢复之前的组件实例(这里简单设置为null也行)
+    setCurrentInstance(null);

		// 如果setup函数返回值是一个函数，那么就将其作为render函数
    if (typeof setupResult === "function") {
      // 如果同时也定义了render函数，报告错误，无论如何给render重新赋值
      if (render) {
        console.error("setup返回渲染函数，render函数将被忽略");
      }
      render = setupResult;
    }
    else { 
      // 如果setup函数返回值是一个对象，那么就将其作为state
      setupState = setupResult;
    }
  }


  //......

  // 调用created生命周期函数
  created && created.call(renderContext);

  effect(() => {
    const subTree = render.call(renderContext, renderContext);
    // 检查组件是否已经被挂载了
    if (!instance.isMounted) {
      // ......
      mounted && mounted.call(renderContext);

      instance.mounted && instance.mounted.forEach((fn:any) => fn.call(renderContext));
    }
    // ......省略
}
```

界面调用示例：

```typescript
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h,ref,onMounted} = VueRuntimeCore 

  const MyComponent = {
    name:'MyComponent',
    setup(){
      onMounted(() => {
        console.log("MyComponent mounted")
      })
    },
    render(){
      return h("div",{},[
        h("header",{},[this.$slots.header()]),
        h("main",{},[this.$slots.main()]),
        h("footer",{},[this.$slots.footer()])
      ])
    }
  }

  const BaseComp = {
    name:'BaseComp',
    setup(){
      onMounted(() => {
        console.log("BaseComp mounted")
      })
    },
    render(){
      return h(MyComponent,{},{
        header(){
          return h("h1",{},'我是标题')
        },
        main(){
          return h("p",{},'我是内容')
        },
        footer(){
          return h("h2",{},'我是底部')
        } 
      })
    }
  }

  render(h(BaseComp),document.getElementById('app'))

</script>
```





