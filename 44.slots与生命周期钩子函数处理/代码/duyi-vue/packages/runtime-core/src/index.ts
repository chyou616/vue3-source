export {
  createRenderer,
  options,
  shouldSetAsProps,
  normalizeClass,
  lis,
  render,
  onMounted
} from "./renderer";
export { watch } from "./apiWatch";
export { h } from "./h";
export { ref } from "@vue/reactivity";
