// 返回一个对象，对象其实就是会暴露给模板数据
// 返回一个函数，这个函数会作为组件的render函数使用
// setup函数有两个参数，props和setup上下文对象，上下文对象里面包含attrs，slots，emit，expose


const Comp1 = {
  setup() {
    return () => {
      return h('div', {}, 'hello world')
    }
  },
  render() { 
    h('div', {}, 'hello world')
  }
}

const Comp2 = {
  setup() { 
    const count = ref(0)
    return {
      count
    }
  }
}

const Comp3 = {
  props: {
    foo:String
  },
  setup(props, context) { 
    console.log(props.foo);
    const { attrs, emit, slots, expose } = context;
    const count = ref(0);

    return {
      count
    }
  },
  render() { 
    return h('div', {}, 'count--->' + this.count);
  }
}