## emit处理

emit用来触发组件的自定义事件，比如，如果有下面的代码：

```typescript
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h,ref} = VueRuntimeCore  

  const MyComponent = {
    props:{
      title:String
    },
    setup(props,ctx){
      const foo = ref('hello world')
      const {emit,attrs,slots,expose} = ctx
      emit('click','hello')
      return {
        foo
      }
    },
    render(){
      return h("div",{},[
        h("h2",{},'标题内容是--->' + this.title),
        h("span",{},'我是文本内容--->' + this.foo.value),
        h("button",{
          onClick:()=>{
            console.log('click')
            this.foo.value = 'hello vue'
          }
        },'按钮')
      ])
    }
  }

  render(h(MyComponent,{
    title:"我是title",
    onClick:(a)=>{
      console.log('click--->' + a)
    }
  }),document.getElementById('app'))
</script>
</html>
```

事件其实我们在之前就已经知道，和属性一样，都是放在`props`中的，只不过我们约定，事件需要以`on`开头，所以在之前的处理中，我们需要加入对props的事件处理

```diff
function resolveProps(options:any, propsData:any) { 
  const props:any = {};
  const attrs:any = {};

  for (const key in propsData) {
+    if (key in options || key.startsWith("on")) {
      props[key] = propsData[key];
    }
    else { 
      attrs[key] = propsData[key];
    }
  }

  return [props, attrs];
}
```

而且`emit`我们绑定在`setupContext`中，这就需要我们去处理一下这个函数

```typescript
function mountComponent(vnode:any, container:any, anchor:any) {
  // ......其他省略

  function emit(event: string, ...payload: any[]) { 
    // 根据事件约定名称进行处理
    const eventName = `on${event[0].toUpperCase() + event.slice(1)}`;
    // 在props中获取事件处理函数，注意resolveProps函数需要处理on开头的事件名称
    const handler = instance.props[eventName];

    if (handler) {
      handler(...payload);
    }
    else { 
      console.error("事件不存在")
    }
  }

  // 定义组件实例，本质就是一个对象，用来存储与组件相关的状态
  const instance = {
    state,
    props:shallowReactive(props),
    isMounted: false,
    subTree: null,
  }

  
  const setupContext = { attrs, emit };
  
  // ...其他省略
}
```

这样，其实我们已经可以在界面触发emit函数了。

不过，如果我们需要自己点击事件去处理的话，现在存在一个问题，渲染函数`render`访问不了`setup`上的`emit`函数，意思如下：

```diff
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h,ref} = VueRuntimeCore  

  const MyComponent = {
    props:{
      title:String
    },
    setup(props,ctx){
      const foo = ref('hello world')
      const {emit,attrs,slots,expose} = ctx
      // emit('click','hello')
      return {
        foo
      }
    },
    render(){
      return h("div",{},[
        h("h2",{
+          onClick:()=>{
+            // 下面这里是访问不到emit函数的
+            emit('click','world')
+          }
        },'标题内容是--->' + this.title),
        h("span",{},'我是文本内容--->' + this.foo.value),
        h("button",{
          onClick:()=>{
            console.log('click')
            this.foo.value = 'hello vue'
          }
        },'按钮')
      ])
    }
  }

  render(h(MyComponent,{
    title:"我是title",
    onClick:(a)=>{
      console.log('click--->' + a)
    }
  }),document.getElementById('app'))
</script>
</html>
```

其实，我们只需要将emit函数也放在代理对象中就可以了。然后界面通过`this`对象去进行访问，而且参照vue的习惯，我们使用`this.$emit`去访问emit函数

```diff
function mountComponent(vnode:any, container:any, anchor:any) {
  // .....省略其他代码
  const instance = {
    state,
    props:shallowReactive(props),
    isMounted: false,
    subTree: null,
+    emit
  }

  // ......
  const setupContext = { attrs, emit };

  // ......

  const renderContext = new Proxy(instance, {
    get(target, key, receiver) {
+      const { state, props, emit } = target;
+      if (key === '$emit') return emit

      if(state && key in state) {
        return state[key];
      }
      else if(key in props) {
        return props[key];
      }
      else if (setupState && key in setupState) {
        // 渲染上下文需要增加对setupState的支持
        return setupState[key];
      }
      else {
        console.error("不存在")
      }
    },
    set(target, key, value, receiver) {

    }
  })
}
```

