import { TrackOpTypes, TriggerOpTypes } from "./operations";

let shouldTrack = true;

export function pauseTracking() { 
  shouldTrack = false;
}

export function enableTracking() { 
  shouldTrack = true;
}

export function track(target: object, type: TrackOpTypes, key: unknown) { 
  if (!shouldTrack) { 
    return;
  }
  console.log(`依赖收集：【${type}】 ${String(key)}属性被读取了`);
}

export function trigger(target: object, type: TriggerOpTypes, key: unknown) {
  console.log(`触发更新：【${type}】 ${String(key)}属性被修改了`);
}