## setup函数

[组合式 API：setup()](https://cn.vuejs.org/api/composition-api-setup.html)

对于我们现在来说，总结setup()函数就下面几点：

- 可以**返回一个对象**，对象中包含的数据会暴露给模板
- 可以**返回一个函数**，这个函数其实会作为组件的render函数使用
- setup函数有两个参数，一个是`props`，另外一个 **Setup 上下文**对象，其实主要包含`attrs`, `slots`, `emit`, `expose`这几个属性，需要注意的是props是具有响应式的，并且是只读的，而`attrs`和`slots`属性都不是响应式的

基本的形式：

```typescript
const Comp1 = {
  setup(){
    return ()=>{
      return h('div',{},'hello world')
    }
  }
}

const Comp2 = {
  setup(){
    const count = ref(0);
    return {
      count
    }
  },
  render(){
    return h("div",{},'count--->' + this.count)
  }
}

const Comp3 = {
  props:{
    foo:String
  },
  setup(props, context){
    console.log(props.foo)
    const {attrs, emit, slots, expose} = context
    const count = ref(0);
    return {
      count
    }
  },
  render(){
    return h("div",{},'count--->' + this.count)
  }
}
```

比如，将前面的代码修改一下，改成下面的样子：

```typescript
<body>
  <div id="app"></div>
</body>
<script src="../dist/runtime-core.global.js"></script>
<script>
  const {render,h,ref} = VueRuntimeCore
  const MyComponent = {
    props:{
      title:String
    },
    setup(props,ctx){
      const foo = ref('hello world')
      const {emit,attrs,slots,expose} = ctx
      return {
        foo
      }
    },
    render(){
      return h("div",{},[
        h("h2",{},'标题内容是--->' + this.title),
        h("span",{},'我是文本内容--->' + this.foo.value),
        h("button",{
          onClick:()=>{
            console.log('click')
            this.foo.value = 'hello vue'
          }
        },'按钮')
      ])
    }
  }

  render(h(MyComponent,{title:"我是title"}),document.getElementById('app'))
</script>
```

现在要让这样的代码代码生效，我们需要做几方面的修改，当然对于`mountCompoent`函数的修改是肯定的，因为我们肯定需要加入对于`setup`函数的处理。

## ref函数的引入与错误屏蔽

不过，首先有个小地方要处理一下，我们在`VueRuntimeCore`对象中，解构出了`ref`，这个其实好办，在`runtime-core`项目的`index.ts`中，直接导出`ref`即可

```typescript
// runtime-core/src/index.ts
// ......
export { ref } from "@vue/reactivity"
```

不过这样，我们在打包的时候可能会报出错误提示。

```
Error: /Users/yingside/work/temp/duyi-vue/packages/reactivity/src/operations.ts:1:1 - (ae-wrong-input-file-type) Incorrect file type; API Extractor expects to analyze compiler outputs with the .d.ts file extension. Troubleshooting tips: https://api-extractor.com/link/dts-error
```

这其实是`@microsoft/api-extractor`包的问题，在整合.d.ts文件的时候报出的错误提示，这个我们不需要管他，其实可以正确的执行，只是这个错误提示太烦，我们在**根目录下的`api-extractor.json`**加入下面的代码，屏蔽掉这个错误提示就行了。

```diff
"extractorMessageReporting": {
  "default": {
    "logLevel": "warning",
    "addToApiReportFile": true
  },
+  "ae-wrong-input-file-type": {
+    "logLevel": "none"
+  },
  "ae-missing-release-tag": {
    "logLevel": "none"
  }
},
```

## 加入setup函数的处理

接下其实就是在mountComponent函数中，加入对于setup函数的处理就行了

```typescript
function mountComponent(vnode:any, container:any, anchor:any) {
  const componentOptions = vnode.type;
  let { render, data, setup, props:propsOption, beforeCreate, created, beforeMount, mounted, beforeUpdate, updated  } = componentOptions;

  // 调用beforeCreate生命周期函数
  beforeCreate && beforeCreate();

  const state = data ? reactive(data()) : null;

  // 调用resolveProps函数，将propsData分解为props和attrs
  const [props, attrs] = resolveProps(propsOption, vnode.props);

  // 定义组件实例，本质就是一个对象，用来存储与组件相关的状态
  const instance = {
    state,
    props:shallowReactive(props),
    isMounted: false,
    subTree: null,
  }

  // 创建setupContext对象，这里面应该有attrs，slots，emit，expose等属性，现在只有attrs
  const setupContext = { attrs };

  // 调用setup函数，将只读的props作为第一个参数传递，避免用户意外的修改props
  // 将setupContext作为第二个参数传递
  const setupResult = setup(shallowReadonly(instance.props), setupContext);

  // 存储setup函数返回的数据
  let setupState = null;

  // 如果setup函数返回值是一个函数，那么就将其作为render函数
  if (typeof setupResult === "function") {
    // 如果同时也定义了render函数，报告错误，无论如何给render重新赋值
    if (render) {
      console.error("setup返回渲染函数，render函数将被忽略");
    }
    render = setupResult;
  }
  else { 
    // 如果setup函数返回值是一个对象，那么就将其作为state
    setupState = setupResult;
  }

  // 将instance挂载到vnode上
  vnode.component = instance;

  const renderContext = new Proxy(instance, {
    get(target, key, receiver) {
      const { state, props } = target;
      if(state && key in state) {
        return state[key];
      }
      else if(key in props) {
        return props[key];
      }
      else if (setupState && key in setupState) {
        // 渲染上下文需要增加对setupState的支持
        return setupState[key];
      }
      else {
        console.error("不存在")
      }
    },
    set(target, key, value, receiver) {
      const { state, props } = target;
      if (state && key in state) {
        state[key] = value;
      }
      else if (key in props) { 
        console.warn("props是只读的")
      }
      else if (setupState && key in setupState) {
        setupState[key] = value;
      }
      else { 
        console.error("不存在")
      }
      return true;
    }
  })

  // 调用created生命周期函数
  created && created.call(renderContext);

  effect(() => {
    const subTree = render.call(renderContext, renderContext);
    // 检查组件是否已经被挂载了
    if (!instance.isMounted) {
      // 调用beforeMount生命周期函数
      beforeMount && beforeMount.call(renderContext);

      // 初次挂载，patch函数第一个参数当然为null
      patch(null, subTree, container, anchor);
      // 挂载完成后，将isMounted设置为true
      instance.isMounted = true;

      // 调用mounted生命周期函数
      mounted && mounted.call(renderContext);
    }
    else {

      // 调用beforeUpdate生命周期函数
      beforeUpdate && beforeUpdate.call(renderContext);

      // 第一个参数为组件上一次渲染的subTree
      patch(instance.subTree, subTree, container, anchor);

      // 调用updated生命周期函数
      updated && updated.call(renderContext);
    }
    // 更新subTree
    instance.subTree = subTree;

  }, {
    scheduler: queueJob
  })
}
```

## shallowReadonly的实现

不过要注意，上面只读浅响应式的api，`shallowReadonly`我们之前并没有实现，现在实现一下也非常简单。

在`reactivity`项目的`baseHandlers.ts`文件中，加入下面的代码：

```typescript
// 其他省略
const shallowReadonlyGet = /*#__PURE__*/ createGetter(true, true)

export const shallowReadonlyHandlers: ProxyHandler<object> = extend(
  {},
  readonlyHandlers,
  {
    get: shallowReadonlyGet
  }
)
```

在reactive.ts中处理：

```typescript
import { shallowReadonlyHandlers } from "./baseHandlers";

// 其他省略......
export function shallowReadonly<T extends object>(
  target: T
): Readonly<T> {
  return createReactiveObject(
    target,
    true,
    shallowReadonlyHandlers
  )
}
```

当然，最后在index.ts中导出即可

```typescript
export { reactive,shallowReactive,shallowReadonly } from "./reactive";
```

