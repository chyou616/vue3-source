## Ref的TS改造

### 1、Ref的基本处理

基本的逻辑分析完了，接下来，我们就把简单书写的代码融入到我们的ts代码中，我们可以单独创建`ref.ts`文件。

而且现在我们进入有专门的`ref`处理，之前在`computed`中临时创建的`ref`接口，我们最好放在当前`ref.ts`文件中进行声明，在`computed`中进行引入就行了

```typescript
export interface Ref<T = any> {
  value: T
}
```

在之前的代码中我们处理ref非常的简单：

```typescript
function ref(value) {
  const wrapper = {
    value,
  };
  Object.defineProperty(wrapper, "__v_isRef", {
    value: true,
    writeable: false,
  });
  return reactive(wrapper);
}
```

设置`__v_isRef`属性为`true`，然后给原始值包裹一层对象，直接交给了`reactive`处理，那对于我们现在的改造中还需要概率更多的边界问题，比如：

**1、传入的参数已经是一个ref**

**2、如果传入的参数可以对象也可以是原始值**

**3、shallowRef浅响应式的处理**

对于浅响应式的处理，我们可以直接参考reactive工厂函数的实现，对于已经是`ref`，我们可以封装`isRef`函数进行判断

```typescript
export function isRef<T>(r: Ref<T> | unknown): r is Ref<T>
export function isRef(r: any): r is Ref {
  return Boolean(r && r.__v_isRef === true)
}

function createRef(rawValue: unknown, shallow = false) {
  // 如果 rawValue 是 ref 对象，直接返回
  if (isRef(rawValue)) {
    return rawValue
  }
 	// 其他情况，我们通过RefImpl类来实现
  return new RefImpl(rawValue, shallow)
}
```

这个RefImpl类中，我们肯定需要value，以及`__v_isRef`属性

```typescript
class RefImpl<T> {
  private _value: T

  public readonly __v_isRef = true
	//......
}
```

如果我们需要浅响应式，那直接在ref中进行处理，如果不是，那直接交给`reactive`

```typescript
const convert = <T extends unknown>(val: T): T =>
  isObject(val) ? reactive(val) : val

class RefImpl<T> {
  private _value: T

  public readonly __v_isRef = true

  constructor(private _rawValue: T, private readonly _shallow = false) {
    this._value = _shallow ? _rawValue : convert(_rawValue)
  }
}
```

同时我们还是要考虑**`_rawValue`是原始值还是对象**，所以`convert`函数的作用是将传入的值 `val` 转换为 Vue 3 的响应式对象，但只有当 `val` 是对象时才会这样做。如果 `val` 不是对象，则直接返回它自己。

既然原始值需要自行处理，那么当然我们在下面需要加上`getter`和`setter`访问器属性，访问器属性中我们对`'value'`属性进行拦截

```typescript
class RefImpl<T> {
  private _value: T

  public readonly __v_isRef = true

  constructor(private _rawValue: T, private readonly _shallow = false) {
    this._value = _shallow ? _rawValue : convert(_rawValue)
  }

  get value() {
    track(toRaw(this), TrackOpTypes.GET, 'value')
    return this._value
  }

  set value(newVal) {
    if (hasChanged(toRaw(newVal), this._rawValue)) {
      this._rawValue = newVal
      this._value = this._shallow ? newVal : convert(newVal)
      trigger(toRaw(this), TriggerOpTypes.SET, 'value', newVal)
    }
  }
}
```

然后，我们通过`ref`和`shallowRef`调用`createRef`即可。不过我们这里对于ts的处理就简单化了...vue源码中对于`ref`的`ts`处理相当复杂，因为涉及到获取`ref`中保存的真正类型，这得单独出几节课才能说的清楚。直接`any`吧。

```typescript
export function ref(value?: any): any {
  return createRef(value);
}

export function shallowRef(value?: any): any {
  return createRef(value, true);
}
```

**题外话（待处理）：**

**为了后面代码方便**，既然上面有判断Ref的函数，那么我们也可以加上判断是否是Reactive的函数和readonly的函数

**reactive.ts**

```typescript
export function isReactive(value: unknown): boolean {
  if (isReadonly(value)) {
    return isReactive((value as Target)[ReactiveFlags.RAW])
  }
  return !!(value && (value as Target)[ReactiveFlags.IS_REACTIVE])
}

export function isReadonly(value: unknown): boolean {
  return !!(value && (value as Target)[ReactiveFlags.IS_READONLY])
}
```

### 2、toRef和toRefs的处理

接下来要处理响应式丢失问题，toRef我们之前有**两个参数**，**一个是传入的任意对象，一个是该对象的key**，首先任意对象当然我们可以用泛型，其次对于TS而言，**第二个参数的key应该来时第一个参数**

**最后的返回值，应该是被Ref包裹的，第一个对象参数，key属性的值**

```typescript
function toRef<T extends object, K extends keyof T>(
  object: T,
  key: K
) :Ref<T[K]> {
  // ......
}
```

在具体处理上，我们的判断更加完善一些，如果传入的`object[key]`已经是一个Ref，那么没有必要再做响应式的相关处理，不是我们再处理。

```typescript
function toRef<T extends object, K extends keyof T>(
  object: T,
  key: K
): Ref<T[K]> {
  return isRef(object[key])
    ? object[key]
    : (new ObjectRefImpl(object, key) as any);
}

class ObjectRefImpl<T extends object, K extends keyof T> {
  public readonly __v_isRef = true;

  constructor(private readonly _object: T, private readonly _key: K) {}

  get value() {
    return this._object[this._key];
  }

  set value(newVal) {
    this._object[this._key] = newVal;
  }
}
```

同样，toRefs还需要再进行简单处理

```typescript
export type ToRefs<T = any> = { [K in keyof T]: Ref<T[K]> }

export function toRefs<T extends object>(object: T): ToRefs<T> { 
  const ret:any = isArray(object) ? new Array(object.length) : {};
  for (const key in object) { 
    ret[key] = toRef(object, key);
  }
  return ret;
}
```

### 3、自动脱ref的处理

上面写的proxyRefs，实际就是又返回了一个`Proxy`代理对象，根据传入的`ref`,当然也有可能不是`ref`，直接返回带value的值，或者是直接返回。

```typescript
export function proxyRefs<T extends object>(
  objectWithRefs: T
): ShallowUnwrapRef<T> {
  // ......
}

export type ShallowUnwrapRef<T> = {
  [K in keyof T]: T[K] extends Ref<infer V> ? V : T[K]
}
```

`T[K] extends Ref<infer V> ? V : T[K]`，如果属性的值是继承自Ref，那么直接推断`infer V`,那就返回V的类型，如果不是Ref，那么就返回当前值的类型.

那接下来里面的内容就很简单了，如果就是getter判断是否是Ref，然后返回，setter同理，如果是Ref，直接给value设置值。

不过TS中按照源码，我们稍微封装一下。

```typescript
export function proxyRefs<T extends object>(
  objectWithRefs: T
): ShallowUnwrapRef<T> {
  return new Proxy(objectWithRefs, shallowUnwrapHandlers)
}

export function unref<T>(ref: T): T extends Ref<infer V> ? V : T {
  return isRef(ref) ? (ref.value as any) : ref
}

const shallowUnwrapHandlers: ProxyHandler<any> = {
  get: (target, key, receiver) => unref(Reflect.get(target, key, receiver)),
  set: (target, key, value, receiver) => {
    const oldValue = target[key]
    if (isRef(oldValue) && !isRef(value)) {
      oldValue.value = value
      return true
    } else {
      return Reflect.set(target, key, value, receiver)
    }
  }
}
```

