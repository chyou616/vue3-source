## trigger相关的改造

基本上来说，我们现在只需要把之前的代码复制粘贴进来，**稍微改一改变量名，加上基本的类型标注**就行了

```typescript
export function trigger(target: object, type: TriggerOpTypes, key: unknown) {
  // 根据target从buckets中获取depsMap
  const depsMap = targetMap.get(target);
  // 如果depsMap不存在,则直接返回
  if (!depsMap) {
    return;
  }
  // 根据key从depsMap中获取effects
  const deps = depsMap.get(key);

  // 执行effects中的副作用函数
  // 为了避免无限循环,这里新建了一个Set对象
  const effects = new Set<ReactiveEffect>();

  deps &&
    deps.forEach((effectFn) => {
      // 如果当前副作用函数不是当前激活的副作用函数,则将其添加到effectsToRun中
      if (effectFn !== activeEffect) {
        effects.add(effectFn);
      }
    });

  // 取得与ITERATE_KEY相关的副作用函数
  const iterateEffects = depsMap.get(ITERATE_KEY);
  // 将与ITERATE_KEY相关的副作用函数也添加到effectsToRun中
  iterateEffects &&
    iterateEffects.forEach((effectFn) => {
      if (effectFn !== activeEffect) {
        effects.add(effectFn);
      }
    });

    effects.forEach((effect) => {
    // 如果effect.options.schedular存在,则执行effect.options.schedular
    // 并将副作用函数作为参数传递进去
    if (effect.options.scheduler) {
      effect.options.scheduler(effect);
    } else {
      // 否则直接执行副作用函数(之前的默认行为)
      effect();
    }
  });
}
```

现在已经能和之前的代码结合到一起了，我们写个例子测试一下：

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <div id="layer1"></div>
  <div id="layer2"></div>
  <button id="btn1">修改1</button>
  <button id="btn2">修改2</button>
</body>
</html>
```

修改一下index.ts中的代码：

```typescript
import { effect } from "./effect";
import { reactive } from "./reactive";

const layer1 = document.querySelector("#layer1")!;
const layer2 = document.querySelector("#layer2")!;
const btn1 = document.querySelector("#btn1")!;
const btn2 = document.querySelector("#btn2")!;

const arr = ["jack", "lucy", "lily"];
const obj = {
  name: "jack",
  age: 18,
  addr: {
    province: "四川",
    city: "成都"
  }
}
const stateArr = reactive(arr);
const stateObj = reactive(obj);
function fn() { 
  console.log("---执行了函数fn---")
  layer1.innerHTML = stateArr[0] + "---" + stateArr[1] + "---" + stateArr[2];
  layer2.innerHTML = stateObj.addr.province + "---" + stateObj.addr.city;
}

effect(fn)

btn1.addEventListener("click", () => {
  stateArr[0] = "tom";
})
btn2.addEventListener("click", () => {
  stateObj.addr.province = "湖北";
  stateObj.addr.city = "武汉";
})
```

现在点击按钮，界面代码进行正确的依赖收集和更新。

接下来，我们需要将这里的代码和之前已经写过的代理的代码动一下大手术，很明显，现在trigger中的动作操作，比如ADD，SET，DELETE这些我们都还没有进行分别处理，这样会影响代码的性能和出现一些bug

之前在写代理中对于数组处理的时候，我们分析过几种情况：

**1、数组`length`属性被隐式的修改了**

当数组在写入值的时候，某个位置上有值，当然是set，某个位置上没有值，是add，这些都没有问题。但是仔细思考`state1[10] = 77`应该是少了一种操作，因为我们这样做之后，**数组的长度是发生了变化的，数组的长度变化，肯定会对相应的调用产生影响**

```typescript
const arr1 = [1, 2, , 4, 5, 6];
const state1 = reactive(arr1);
state1[0] = 99; // set
state1[2] = 100; // add
state1[10] = 77; // add
```

我们**当时的做法是判断传入的是否是数组，并且值有没有发生变化，而且key不能是length，毕竟是隐式修改嘛**，当然这是没有任何问题的，我们现在也应该这样去思考。

只不过，之前我们并没有加入trigger具体代码的实现，所以在set函数中处理的判断就稍显啰嗦，其实现在我们完全可以把这部分的处理就交给trigger去进行处理了，set函数这边我们简单判断就行。

**2、直接通过`length`修改了数组的长度**

```typescript
const arr1 = [1, 2, 3, 4, 5, 6];
const state1 = reactive(arr1);
state1.length = 3;
```

### 1、ADD和SET操作的判断处理

**baseHandlers.ts**

```typescript
function createSetter(shallow = false) { 
  return function set(
    target: Record<string | symbol, unknown>,
    key: string | symbol,
    value: unknown,
    receiver: object
  ): boolean {
  let oldValue = target[key];
  // 如果目标是数组，并且 key 是一个有效的数组索引，则判断 key 是否小于数组的长度（即是否为有效索引）。
  // 如果目标是普通对象或其他非数组对象，则判断对象是否有这个 key
  // const hadKey = 如果是数组, 并且 key 是一个有效的数组索引
  //   ? key 是否小于数组的长度(小于Set操作，大于Add操作) 
  //   : 不是数组直接判断是否有这个属性(有Set操作，没有Add操作)
  const hadKey =
    isArray(target) && isIntegerKey(key)
      ? Number(key) < target.length
      : hasOwn(target, key)
    
  }


```

**utils.ts**

```typescript
......
// 判断一个 key 是否是一个合法的整数类型的字符串
export const isIntegerKey = (key: unknown) =>
  isString(key) &&                // 检查 key 是否是字符串
  key !== 'NaN' &&                // 确保 key 不是字符串 'NaN'
  key[0] !== '-' &&               // 确保 key 不是负数（即 key 的第一个字符不是 '-')
  '' + parseInt(key, 10) === key;  // 确保 key 是一个可以被转换为整数的合法字符串

// hasOwnProperty 检查对象自身是否拥有某个属性，而不是从其原型链继承来的属性
// key is keyof typeof val 表示 key 是 val 的一个键, TS方法的谓语动词，
// 目的是为了在调用方法的时候也能够进行类型收窄。
// 因为TS的类型推断是基于值的，而不是基于变量的，
// 在调用方法的时候，TS无法推断出方法的返回值，所以我们需要使用谓语动词来告诉TS方法的返回值的类型。
const hasOwnProperty = Object.prototype.hasOwnProperty
export const hasOwn = (
  val: object,                    // 第一个参数 val 是一个对象
  key: string | symbol            // 第二个参数 key 是一个字符串或 symbol，表示属性的键
): key is keyof typeof val => hasOwnProperty.call(val, key)
```

### 2、确保只是对代理对象进行响应式更新处理

有可能出现下面的情况：

```typescript
const obj = reactive({ a: 1 })
const child = Object.create(obj)
child.a = 2
```

在这个例子中，`child` 是通过 `Object.create` 创建的，它的原型链上有 `obj`。在操作 `child` 的属性时，`child` 的原型链上 `obj` 的属性也可能会被访问到。

如果在 `child` 上操作 `a`，不应该触发 `obj` 的更新，因为 `child` 并不是响应式的（它只是继承了 `obj` 的属性）。

所以，我们在处理之前，先进行判断

而且，现在我们需要通过`trigger`再做一些对值的操作，因此，我们把`newValue`和`oldValue`当做新的参数传入进去，方便我们后续的操作

```typescript
// 这个判断的主要目的是确保我们只对当前的响应式对象触发响应式更新，
// 而不会对其原型链上的对象或者代理的子对象进行重复或不必要的更新通知。
if (target === toRaw(receiver)) { 
  if (!hadKey) { // ADD操作
    trigger(target, TriggerOpTypes.ADD, key, value)
  } else if (hasChanged(value, oldValue)) { // SET操作
    trigger(target, TriggerOpTypes.SET, key, value, oldValue)
  }
}
```

### 3、对一般情况属性key的处理

阅读我们之前的代码，可以发现，针对一般情况对象属性的`key`，或者是针对迭代的`ITERATE_KEY`其实**都需要对Set集合进行forEach操作，把符合情况的副作用函数加入到新的Set集合中**，那么我们干脆把这个forEach操作进行封装

**trigger.ts**

```typescript
export function trigger(
	target: object,
  type: TriggerOpTypes,
  key: unknown,
  newValue?: unknown,
  oldValue?: unknown) {
  // ...其他代码省略
  const effects = new Set<ReactiveEffect>();
  const add = (effectsToAdd: Set<ReactiveEffect> | undefined) => {
    if (effectsToAdd) {
      effectsToAdd.forEach(effect => {
        if (effect !== activeEffect) {
          effects.add(effect)
        }
      })
    }
  }

  // 普通对象属性key
  if (key !== void 0) { 
    add(depsMap.get(key))
  }
  // ...其他代码省略
}
```

这里为什么是`key !== void 0`因为其实判断`key !== undefined`就可以了。但是在框架中，需要避免一些极端情况，比如在ES5之前，`undefined` 不是一个保留字，也不是不可变的，因此它是可以被重新定义的。所以使用 `void 0` 来确保得到真正的 `undefined`

### 4、对隐式修改数组`length`与`for...in`的处理

当我们修改某些属性时，还需要进行进一步的思考，这在我们前面已经讲过了

```typescript
stateArr[5] = "merry"
```

**修改的属性是数组的下标**，但是这会**隐式的影响到数组的长度**，而且也会影响到**for...in循环迭代**

所以对于具体的动作而言：

**`ADD`操作**，会影响`for...in`循环迭代，也会隐式的影响数组长度，这些都需要触发更新

**`DELETE`操作**，会影响`for...in`循环迭代，需要触发更新

**注意`delete arr[1]`这种操作**，仅仅**只是将当前数组的值设置为了`undefined`**，**并不会引发数组长度的改变**。

```typescript
 switch (type) { 
  case TriggerOpTypes.ADD:
    // 如果不是数组，证明是需要迭代的对象
    if (!isArray(target)) {
      add(depsMap.get(ITERATE_KEY))
    }
    // key是一个整数类型的字符串，证明是数组，需要触发length属性
    else if (isIntegerKey(key)) {
      add(depsMap.get('length'))
    }
    break;
  case TriggerOpTypes.DELETE:
    // 如果不是数组，证明是需要迭代的对象
    if (!isArray(target)) {
      add(depsMap.get(ITERATE_KEY))
    }
    break;
}
```

### 5、直接修改`length`的处理

上面的例子，是属性的修改会隐式的影响数组的属性`length`和`for...in`迭代

那现在反过来思考，当数组的 `length` 被直接修改时，其实也会影响到数组元素。

```typescript
layer1.innerHTML = stateArr[0] + "---" + stateArr[1] + "---" + stateArr[2];
```

那我们这里的例子来说，当`stateArr.length=10`的时候，并不会对数组前面的元素0,1,2有任何影响，因此不用关心触发更新的问题，但是如果`stateArr.length=0`的时候，**所有的元素都被删除了，这对界面肯定会造成影响**，因此需要**触发更新**

也就是说，数组的长度本来是3，我们新传入的值是0，只要循环迭代的索引大于等于0，就应该添加触发更新

数组的长度本来是3，我们新传入的值是1，只要循环迭代的索引大于等于1，就应该添加触发更新

因此：

```typescript
// 当属性key是length的时候，有可能会隐式影响数组的元素
if (key === "length" && isArray(target)) {
  depsMap.forEach((dep, key) => {
    // 当key是length,或者索引大于length时，需要触发副作用函数
    if (key === "length" || key >= (newValue as number)) {
      add(dep);
    }
  });
}
```

完整了`trigger.ts`代码

```typescript
export function trigger(
  target: object,
  type: TriggerOpTypes,
  key: unknown,
  newValue?: unknown,
  oldValue?: unknown
) {
  // 根据target从buckets中获取depsMap
  const depsMap = targetMap.get(target);
  // 如果depsMap不存在,则直接返回
  if (!depsMap) {
    return;
  }

  // 执行effects中的副作用函数
  // 为了避免无限循环,这里新建了一个Set对象
  const effects = new Set<ReactiveEffect>();
  const add = (effectsToAdd: Set<ReactiveEffect> | undefined) => {
    if (effectsToAdd) {
      effectsToAdd.forEach((effect) => {
        if (effect !== activeEffect) {
          effects.add(effect);
        }
      });
    }
  };

  // 当属性key是length的时候，有可能会隐式影响数组的元素
  if (key === "length" && isArray(target)) {
    depsMap.forEach((dep, key) => {
      // 当key是length,或者索引大于length时，需要触发副作用函数
      if (key === "length" || key >= (newValue as number)) {
        add(dep);
      }
    });
  } else {
    // 普通对象属性key
    if (key !== void 0) {
      console.log(type);
      add(depsMap.get(key));
    }

    switch (type) {
      case TriggerOpTypes.ADD:
        // 如果不是数组，证明是需要迭代的对象
        if (!isArray(target)) {
          add(depsMap.get(ITERATE_KEY));
        }
        // key是一个整数类型的字符串，证明是数组，需要触发length属性
        else if (isIntegerKey(key)) {
          add(depsMap.get("length"));
        }
        break;
      case TriggerOpTypes.DELETE:
        // 如果不是数组，证明是需要迭代的对象
        if (!isArray(target)) {
          add(depsMap.get(ITERATE_KEY));
        }
        break;
    }
  }

  effects.forEach((effect) => {
    // 如果effect.options.schedular存在,则执行effect.options.schedular
    // 并将副作用函数作为参数传递进去
    if (effect.options.scheduler) {
      effect.options.scheduler(effect);
    } else {
      // 否则直接执行副作用函数(之前的默认行为)
      effect();
    }
  });
}
```
